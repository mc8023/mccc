package com.mc.mccc.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.mc.mccc.enter.SysRole;
import com.mc.mccc.mapper.BaseMapper;
import com.mc.mccc.mapper.SysRoleMapper;

@Service
public class SysRoleServiceImpl extends BaseServiceImpl<SysRole, Integer> {

	@Resource
	private SysRoleMapper sysRoleMapper;
	
	@Override
	public BaseMapper getMapper(){
		return sysRoleMapper;
	}

}
