package com.mc.mccc.service.impl;

import java.io.Serializable;
import java.util.List;

import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.mc.mccc.mapper.BaseMapper;
import com.mc.mccc.service.inter.BaseService;
import com.mc.mccc.comm.QueryParamBean;
import com.mc.mccc.comm.bean.PageParamBean;

@Service
public abstract class  BaseServiceImpl<T,ID extends Serializable> implements BaseService<T, ID>{

	public abstract  BaseMapper getMapper();
	
	@Override
	public int add(T record) {
		return getMapper().add(record);
	}

	@Override
	public int deleteById(ID id) {
		return getMapper().deleteById(id);
	}

	@Override
	public int deleteByIds(Integer[] ids) {
		return getMapper().deleteByIds(ids);
	}

	@Override
	public int updateBySelective(T record) {
		return getMapper().updateBySelective(record);
	}

	@Override
	public T findById(ID id) {
		return (T) getMapper().findById(id);
	}

	@Override
	public Page<T> findPageList(PageParamBean pageParam) {
		PageHelper.startPage(pageParam.getPageNum(), pageParam.getPageSize());
		return getMapper().findPageList();
	}

	@Override
	public Page<T> findPageListBySelective(PageParamBean pageParam,T record) {
		PageHelper.startPage(pageParam.getPageNum(), pageParam.getPageSize());
		return getMapper().findPageListBySelective(record);
	}
	
	@Override
	public Page<T> findPageListByParamBean(PageParamBean pageParam,QueryParamBean bean) {
		PageHelper.startPage(pageParam.getPageNum(), pageParam.getPageSize());
		return getMapper().findPageListByParamBean(bean);
	}

	@Override
	public List<T> findAllList() {
		return getMapper().findAllList();
	}

	@Override
	public List<T> findListBySelective(T record) {
		return getMapper().findListBySelective(record);
	}
	
	@Override
	public int addBatch(List<T> records) {
		return getMapper().addBatch(records);
	}
	
}
