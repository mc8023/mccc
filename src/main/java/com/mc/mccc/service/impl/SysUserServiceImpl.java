package com.mc.mccc.service.impl;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import com.mc.mccc.mapper.BaseMapper;
import com.mc.mccc.mapper.SysUserMapper;
import com.mc.mccc.dto.Edtpsw;
import com.mc.mccc.enter.SysUser;

@Service
public class SysUserServiceImpl extends BaseServiceImpl<SysUser, Integer> {

	@Resource
	private SysUserMapper sysUserMapper;
	
	public BaseMapper getMapper(){
		return sysUserMapper;
	}

	public boolean usernameIsExists(String username) {
		if(StringUtils.isNotBlank(sysUserMapper.usernameIsExists(username))){
			return true;
		}
		return false;
	}
	
	public SysUser getSysUserByUsername(String username){
	    
	    return sysUserMapper.getSysUserByUsername(username);
	}

	public void updateuserpsw(Edtpsw psw) {
		 sysUserMapper.updateuserpsw(psw);
	}

	public Edtpsw findpzmm() {
		return sysUserMapper.findpzmm();
	}


}
