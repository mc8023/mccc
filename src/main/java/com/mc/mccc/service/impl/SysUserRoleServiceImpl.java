package com.mc.mccc.service.impl;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import com.mc.mccc.comm.Constant;
import com.mc.mccc.enter.SysUserRole;
import com.mc.mccc.mapper.BaseMapper;
import com.mc.mccc.mapper.SysUserRoleMapper;

@Service
public class SysUserRoleServiceImpl extends BaseServiceImpl<SysUserRole, Integer>{

    @Resource
    private SysUserRoleMapper sysUserRoleMapper;
    
    @Override
    public BaseMapper getMapper() {
        return sysUserRoleMapper;
    }
    
    public List<SysUserRole> getUserRolesByUserId(Integer userId){
        return sysUserRoleMapper.getUserRolesByUserId(userId);
    }
    
 	public void addBatchUserRole(Integer[] ids, Integer userId) {
		List<SysUserRole> sruList = new ArrayList<SysUserRole>();
		for (int i = 0; i < ids.length; i++) {
			SysUserRole userRole = new SysUserRole();
			userRole.setRoleId(ids[i]);
			userRole.setUserId(userId);
			userRole.setStatus(Constant.STATUS_EFFECTIVE);
			sruList.add(userRole);
		}
		sysUserRoleMapper.deleteByUserId(userId);
		sysUserRoleMapper.addBatch(sruList);
	}
    
    

}
