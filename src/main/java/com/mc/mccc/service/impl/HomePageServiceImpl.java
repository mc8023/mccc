package com.mc.mccc.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.mc.mccc.comm.QueryParamBean;
import com.mc.mccc.comm.bean.PageParamBean;
import com.mc.mccc.enter.SysMenu;
import com.mc.mccc.mapper.BaseMapper;
import com.mc.mccc.mapper.HomePageMapper;

@Service
public class HomePageServiceImpl extends BaseServiceImpl<SysMenu, Integer> {

	@Resource
	private HomePageMapper homePageMapper;
	
	public BaseMapper getMapper(){
		return homePageMapper;
	}
	
	public List<SysMenu> findSysMenuByUserId(Integer userId){
	    return homePageMapper.findSysMenuByUserId(userId);
	}

	@Override
	public Page<SysMenu> findPageList(PageParamBean pageParam) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Page<SysMenu> findPageListBySelective(PageParamBean pageParam, SysMenu record) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Page<SysMenu> findPageListByParamBean(PageParamBean pageParam, QueryParamBean bean) {
		// TODO Auto-generated method stub
		return null;
	}

}
