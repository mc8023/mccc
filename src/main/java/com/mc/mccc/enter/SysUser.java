package com.mc.mccc.enter;

/**
 * 系统用户实体类
 * @author luomengzhu
 *
 */
public class SysUser {
	
	private Integer id;
	
	private String username;
	
	private String password;
	
	private Integer status; //用户状态 0，可用  1，不可用
	
	private String email;
	
	private String phone;
	
	private String person_level;
	
	private String query_condition;
	
	private String idCard;
	
	private String pym;
	
	private String ids;

	//private List<Company> company;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	private String insuOrgName;

	public String getInsuOrgName() {
		return insuOrgName;
	}

	public void setInsuOrgName(String insuOrgName) {
		this.insuOrgName = insuOrgName;
	}

	public String getPerson_level() {
		return person_level;
	}

	public void setPerson_level(String person_level) {
		this.person_level = person_level;
	}

	public String getQuery_condition() {
		return query_condition;
	}

	public void setQuery_condition(String query_condition) {
		this.query_condition = query_condition;
	}

	public String getIdCard() {
		return idCard;
	}

	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}

	public String getPym() {
		return pym;
	}

	public void setPym(String pym) {
		this.pym = pym;
	}

	public String getIds() {
		return ids;
	}

	public void setIds(String ids) {
		this.ids = ids;
	}
	
	
}
