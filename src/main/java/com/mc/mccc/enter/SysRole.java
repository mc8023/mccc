package com.mc.mccc.enter;


/**
 * 系统角色实体类
 * @author luomengzhu
 *
 */
public class SysRole {
	
	private Integer id;
	
	private String name;
	
	private Integer status;
	
	private String description;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
