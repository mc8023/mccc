package com.mc.mccc.mapper;

import java.io.Serializable;
import java.util.List;

import com.github.pagehelper.Page;
import com.mc.mccc.comm.QueryParamBean;

/**
 * 通用mapper
 * @author tiansheng
 * @param <T>
 * @param <ID>
 */
public interface BaseMapper<T,ID extends Serializable>  {

	public int add(T record);
	
	public int deleteById(ID id);
	
	public int deleteByIds(Integer[] ids);
	
	public int updateBySelective(T record);
	
	public T findById(ID id);
	
	public Page<T> findPageList();
	
	public Page<T> findPageListBySelective(T record);
	
	public Page<T> findPageListByParamBean(QueryParamBean bean);
	
	public List<T> findAllList();
	
	public List<T> findListBySelective(T record);
	
	public int addBatch(List<T> records);
	
	
	
	
}
