package com.mc.mccc.mapper;

import java.util.List;

import com.mc.mccc.enter.SysUserRole;
import com.mc.mccc.mapper.BaseMapper;

public interface SysUserRoleMapper extends BaseMapper<SysUserRole, Integer>{
    
    public List<SysUserRole> getUserRolesByUserId(Integer userId);
    
    void deleteByUserId(Integer userId);

	public void deletejgbyuserid(String id);

}
