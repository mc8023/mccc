package com.mc.mccc.mapper;

import java.io.Serializable;
import java.util.List;

import com.mc.mccc.enter.SysMenu;

public interface HomePageMapper extends BaseMapper<SysMenu, Serializable> {
	/**
	 * 查询当前用户的所有菜单
	 * @param userId
	 * @return 当前用户的所有菜单
	 */
	  public List<SysMenu> findSysMenuByUserId(Integer userId);
}
