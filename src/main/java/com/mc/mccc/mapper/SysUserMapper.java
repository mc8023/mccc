package com.mc.mccc.mapper;

import java.util.List;

import com.mc.mccc.dto.Edtpsw;
import com.mc.mccc.enter.SysUser;

public interface SysUserMapper extends BaseMapper<SysUser, Integer>{
	
	public String usernameIsExists(String username);
	
	public SysUser getSysUserByUsername(String username);

	public int getSysUserlogin(String username, String password);

	public List<SysUser> getuserlist();

	public void insertuser(String str1, String str2);

	public void insertuser(String str1, String str2, String str3, String str4);

	public void insertuser(String str1, String str2, String str3, String str4, String str5);

	public void updateuserpsw(Edtpsw psw);

	public Edtpsw findpzmm();
}
