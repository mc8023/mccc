package com.mc.mccc.shiro.filter;

import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.authz.RolesAuthorizationFilter;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.io.IOException;

/**
 * Shiro角色异或过滤器
 *
 */
public class RolesOrFilter extends RolesAuthorizationFilter {

	public boolean isAccessAllowed(ServletRequest request,
			ServletResponse response, Object mappedValue) throws IOException {
		Subject subject = getSubject(request, response);
		String[] rolesArray = (String[]) mappedValue;

		if ((rolesArray == null) || (rolesArray.length == 0)) {
			return true;
		}

		for (int i = 0; i < rolesArray.length; i++) {
			
			if( subject.hasRole(rolesArray[i]) )
			{
				//用户只要拥有任何一个角色则验证通过
				return true;
			}
		}
		return false;
	}
}
