package com.mc.mccc.shiro.filter;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.authc.FormAuthenticationFilter;
import com.mc.mccc.shiro.ApplicationConstants;

/**
 * 带角色的窗口登录验证过滤器
 * 
 * 
 */
public class FormWithRoleAuthenticationFilter extends FormAuthenticationFilter {

	/**
	 * 错误响应参数名
	 */
	private String errorMessageParam = "errorMessage";

	/**
	 * 生产令牌
	 */
	@Override
	protected AuthenticationToken createToken(ServletRequest request,
			ServletResponse response) {
		String username = getUsername(request);
		String password = getPassword(request);
		boolean rememberMe = isRememberMe(request);
		String host = getHost(request);
		return new UsernamePasswordToken(username, password,
				rememberMe, host);
	}

	/**
	 * 登录成功
	 */
	@Override
	protected boolean onLoginSuccess(AuthenticationToken token,
			Subject subject, ServletRequest request, ServletResponse response)
			throws Exception {
		issueSuccessRedirect(request, response);

		return false;
	}

	protected boolean onLoginFailure(AuthenticationToken token,
			AuthenticationException e, ServletRequest request,
			ServletResponse response) {
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		/**
		 * 登录失败移除用户信息
		 */
		httpRequest.getSession().removeAttribute(
				ApplicationConstants.LOGIN_USER);
		setFailureAttribute(request, e);
		httpRequest.setAttribute(errorMessageParam, e.getMessage());

		return true;
	}


	public String getErrorMessageParam() {
		return errorMessageParam;
	}

	public void setErrorMessageParam(String errorMessageParam) {
		this.errorMessageParam = errorMessageParam;
	}
}
