package com.mc.mccc.shiro;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import org.apache.commons.collections.CollectionUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheException;
import org.apache.shiro.cache.CacheManager;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.mc.mccc.enter.SysUser;
import com.mc.mccc.service.impl.SysUserRoleServiceImpl;
import com.mc.mccc.service.impl.SysUserServiceImpl;

/**
 * 自定义DB Realm实现
 * 
 */
public class AuthorizingRealm extends org.apache.shiro.realm.AuthorizingRealm {

    @Resource
    private SysUserServiceImpl sysUserServiceImpl;
    
    @Resource
    private SysUserRoleServiceImpl sysUserRoleServiceImpl;

	/**
	 * 登录认证
	 */
	protected AuthenticationInfo doGetAuthenticationInfo(
			AuthenticationToken authcToken) throws AuthenticationException {
		UsernamePasswordToken token = (UsernamePasswordToken) authcToken;
		SysUser user = sysUserServiceImpl.getSysUserByUsername(token.getUsername());
		if (user != null) {
			Subject subject = SecurityUtils.getSubject();
			Session session = subject.getSession();
			// 把用户角色放进session
			session.setAttribute(ApplicationConstants.LOGIN_USER, user);
			return new SimpleAuthenticationInfo(user,
					user.getPassword(), getName());
		} else {
			return null;
		}
	}

	/**
	 * 授权
	 */
	protected AuthorizationInfo doGetAuthorizationInfo(
			PrincipalCollection principals) {

		SimpleAuthorizationInfo auth = new SimpleAuthorizationInfo();
		if (principals == null || principals.isEmpty()) {
			return auth;
		}
		
		SysUser user = (SysUser)getAvailablePrincipal(principals);
		if (user != null) {
//		    List<SysUserRole> userRoles = sysUserRoleServiceImpl.getUserRolesByUserId(user.getId());
		    List<String> roles = new ArrayList<String>();
		    /*if(CollectionUtils.isNotEmpty(userRoles)){
		        for(SysUserRole userRole : userRoles){
		            roles.add(SysRoleType.get(userRole.getRoleId()).toString());
		        }
		    }*/
		    roles.add("ADMIN");
			auth.addRoles(roles);
		}
		return auth;
	}

	/**
	 * 清除授权缓存
	 */
	@Override
	public void clearCachedAuthorizationInfo(PrincipalCollection principals) {
		super.setAuthorizationCache(null);
		super.setAuthorizationCachingEnabled(false);
		super.clearCachedAuthorizationInfo(principals);
	}
	
	/**
	 * 清除验证缓存
	 */
	@Override
	public void clearCachedAuthenticationInfo(PrincipalCollection principals) {
		super.setAuthenticationCache(null);
		super.setAuthenticationCachingEnabled(false);
		super.clearCachedAuthenticationInfo(principals);
	}
	
	/**
	 * 
	 * <P>@describe: 清除所有缓存数据与指定的帐户的身份/认同相关  </P>
	 * <P>@param principals  </P>
	 * <P>@see org.apache.shiro.realm.CachingRealm#clearCache(org.apache.shiro.subject.PrincipalCollection)  </P>
	 * <P>@date: 2017年7月21日 下午5:26:38  </P>
	 * <P>@author: wangaogao  </P>
	 * <P>@remark:  </P>
	 */
	public void clearCache(PrincipalCollection principals) {
		super.clearCache(principals);
	}
	
	public void doClearCache(PrincipalCollection principals) {
		super.doClearCache(principals);
	}

	
}
