package com.mc.mccc.dto;

import java.io.Serializable;

/**
 * 理赔明细
 * @author 唐修亮
 *
 */
public class SettlementYearDto implements Serializable{
	private static final long serialVersionUID = 8084847794595964204L;
	private String areaid;
	private String beginClinDate;		//开始日期
	private String insu_org_nm_ab;
	private String insu_org_id;
	private String sajnum;
	private String pbk;
	private String item_nm;	
	private String item_value;	
	private String syear;			//查询条件 年份
	private String smonth;			//月份
	private String dsznmzrc;		//独生子女门诊人次
	private String dsznmzsqje;		//独生子女门诊申请金额
	private String dsznmzpfje;		//独生子女门诊赔付金额
	private String dsznzyrc;		//独生子女住院人次
	private String dsznzysqje;		//独生子女住院申请金额
	private String dsznzypfje;		//独生子女住院赔付金额
	private String gygxmzrc;		//供养关系门诊人次
	private String gygxmzsqje;		//供养关系门诊申请金额
	private String gygxmzpfje;		//供养关系门诊赔付金额
	private String gygxzyrc;		//供养关系住院人次
	private String gygxzysqje;		//供养关系住院申请金额
	private String gygxzypfje;		//供养关系住院赔付金额
	private String ygywmzrc;		//员工意外门诊人次
	private String ygywmzsqje;		//员工意外门诊申请金额
	private String ygywmzpfje;		//员工意外门诊赔付金额
	private String ygywzyrc;		//员工意外住院人次
	private String ygywzysqje;		//员工意外住院申请金额
	private String ygywzypfje;		//员工意外住院赔付金额	
	private String ygywshrc;		//员工意外伤害人次
	private String ygywshsqje;		//员工意外伤害申请金额
	private String ygywshpfje;		//员工意外伤害赔付金额
	private String zjrc;			//总计认次
	private String zjsqje;			//总计申请金额
	private String zjpfje;			//总计赔付金额
	private String bz;				//备注
	private String hjdsznmzrc;		//独生子女门诊人次
	private String hjdsznmzsqje;	//合计独生子女门诊申请金额
	private String hjdsznmzpfje;	//合计独生子女门诊赔付金额
	private String hjdsznzyrc;		//合计独生子女住院人次
	private String hjdsznzysqje;	//合计独生子女住院申请金额
	private String hjdsznzypfje;	//合计独生子女住院赔付金额
	private String hjgygxmzrc;		//合计供养关系门诊人次
	private String hjgygxmzsqje;	//合计供养关系门诊申请金额
	private String hjgygxmzpfje;	//合计供养关系门诊赔付金额
	private String hjgygxzyrc;		//合计供养关系住院人次
	private String hjgygxzysqje;	//合计供养关系住院申请金额
	private String hjgygxzypfje;	//合计供养关系住院赔付金额
	private String hjygywmzrc;		//合计员工意外门诊人次
	private String hjygywmzsqje;	//合计员工意外门诊申请金额
	private String hjygywmzpfje;	//合计员工意外门诊赔付金额
	private String hjygywzyrc;		//合计员工意外住院人次
	private String hjygywzysqje;	//合计员工意外住院申请金额
	private String hjygywzypfje;	//合计员工意外住院赔付金额	
	private String hjygywshrc;		//合计员工意外伤害人次
	private String hjygywshsqje;	//合计员工意外伤害申请金额
	private String hjygywshpfje;	//合计员工意外伤害赔付金额
	private String hjzjrc;			//合计总计认次
	private String hjzjsqje;		//合计总计申请金额
	private String hjzjpfje;		//合计总计赔付金额
	private String ssqfy;
	private String spffy;
	private String lplx;
	private String rczj;
	private String sqjezj;
	private String pfjezj;
	private long userid;
	
	private String person_level;//查询级别
	
	private String query_condition;//查询条件
	private String mth;
		public String getMth() {
		return mth;
	}

	public void setMth(String mth) {
		this.mth = mth;
	}

		public String getLplx() {
		return lplx;
	}

	public void setLplx(String lplx) {
		this.lplx = lplx;
	}

		public String getInsu_org_id() {
			return insu_org_id;
		}

		public void setInsu_org_id(String insu_org_id) {
			this.insu_org_id = insu_org_id;
		}

		public void setInsu_org_nm_ab(String insu_org_nm_ab) {
			this.insu_org_nm_ab = insu_org_nm_ab;
		}
	    public String getSajnum() {
			return sajnum;
		}

		public void setSajnum(String sajnum) {
			this.sajnum = sajnum;
		}
	public String getBeginClinDate() {
		return beginClinDate;
	}
	public void setBeginClinDate(String beginClinDate) {
		this.beginClinDate = beginClinDate;
	}
	public String getAreaid() {
		return areaid;
	}
	public void setAreaid(String areaid) {
		this.areaid = areaid;
	}
	public String getPbk() {
		return pbk;
	}
	public void setPbk(String pbk) {
		this.pbk = pbk;
	}
	public String getItem_nm() {
		return item_nm;
	}
	public void setItem_nm(String item_nm) {
		this.item_nm = item_nm;
	}
	public String getItem_value() {
		return item_value;
	}
	public void setItem_value(String item_value) {
		this.item_value = item_value;
	}
	public String getSsqfy() {
		return ssqfy;
	}
	public void setSsqfy(String ssqfy) {
		this.ssqfy = ssqfy;
	}
	public String getSpffy() {
		return spffy;
	}
	public void setSpffy(String spffy) {
		this.spffy = spffy;
	}
	public String getSyear() {
		return syear;
	}
	public void setSyear(String syear) {
		this.syear = syear;
	}
	public String getSmonth() {
		return smonth;
	}
	public void setSmonth(String smonth) {
		this.smonth = smonth;
	}
	public String getDsznmzrc() {
		return dsznmzrc;
	}
	public void setDsznmzrc(String dsznmzrc) {
		this.dsznmzrc = dsznmzrc;
	}
	public String getDsznmzsqje() {
		return dsznmzsqje;
	}
	public void setDsznmzsqje(String dsznmzsqje) {
		this.dsznmzsqje = dsznmzsqje;
	}
	public String getDsznmzpfje() {
		return dsznmzpfje;
	}
	public void setDsznmzpfje(String dsznmzpfje) {
		this.dsznmzpfje = dsznmzpfje;
	}
	public String getDsznzyrc() {
		return dsznzyrc;
	}
	public void setDsznzyrc(String dsznzyrc) {
		this.dsznzyrc = dsznzyrc;
	}
	public String getDsznzysqje() {
		return dsznzysqje;
	}
	public void setDsznzysqje(String dsznzysqje) {
		this.dsznzysqje = dsznzysqje;
	}
	public String getDsznzypfje() {
		return dsznzypfje;
	}
	public void setDsznzypfje(String dsznzypfje) {
		this.dsznzypfje = dsznzypfje;
	}
	public String getGygxmzrc() {
		return gygxmzrc;
	}
	public void setGygxmzrc(String gygxmzrc) {
		this.gygxmzrc = gygxmzrc;
	}
	public String getGygxmzsqje() {
		return gygxmzsqje;
	}
	public void setGygxmzsqje(String gygxmzsqje) {
		this.gygxmzsqje = gygxmzsqje;
	}
	public String getGygxmzpfje() {
		return gygxmzpfje;
	}
	public void setGygxmzpfje(String gygxmzpfje) {
		this.gygxmzpfje = gygxmzpfje;
	}
	public String getGygxzyrc() {
		return gygxzyrc;
	}
	public void setGygxzyrc(String gygxzyrc) {
		this.gygxzyrc = gygxzyrc;
	}
	public String getGygxzysqje() {
		return gygxzysqje;
	}
	public void setGygxzysqje(String gygxzysqje) {
		this.gygxzysqje = gygxzysqje;
	}
	public String getGygxzypfje() {
		return gygxzypfje;
	}
	public void setGygxzypfje(String gygxzypfje) {
		this.gygxzypfje = gygxzypfje;
	}
	public String getYgywmzrc() {
		return ygywmzrc;
	}
	public void setYgywmzrc(String ygywmzrc) {
		this.ygywmzrc = ygywmzrc;
	}
	public String getYgywmzsqje() {
		return ygywmzsqje;
	}
	public void setYgywmzsqje(String ygywmzsqje) {
		this.ygywmzsqje = ygywmzsqje;
	}
	public String getYgywmzpfje() {
		return ygywmzpfje;
	}
	public void setYgywmzpfje(String ygywmzpfje) {
		this.ygywmzpfje = ygywmzpfje;
	}
	public String getYgywzyrc() {
		return ygywzyrc;
	}
	public void setYgywzyrc(String ygywzyrc) {
		this.ygywzyrc = ygywzyrc;
	}
	public String getYgywzysqje() {
		return ygywzysqje;
	}
	public void setYgywzysqje(String ygywzysqje) {
		this.ygywzysqje = ygywzysqje;
	}
	public String getYgywzypfje() {
		return ygywzypfje;
	}
	public void setYgywzypfje(String ygywzypfje) {
		this.ygywzypfje = ygywzypfje;
	}
	public String getYgywshrc() {
		return ygywshrc;
	}
	public void setYgywshrc(String ygywshrc) {
		this.ygywshrc = ygywshrc;
	}
	public String getYgywshsqje() {
		return ygywshsqje;
	}
	public void setYgywshsqje(String ygywshsqje) {
		this.ygywshsqje = ygywshsqje;
	}
	public String getYgywshpfje() {
		return ygywshpfje;
	}
	public void setYgywshpfje(String ygywshpfje) {
		this.ygywshpfje = ygywshpfje;
	}
	public String getZjrc() {
		return zjrc;
	}
	public void setZjrc(String zjrc) {
		this.zjrc = zjrc;
	}
	public String getZjsqje() {
		return zjsqje;
	}
	public void setZjsqje(String zjsqje) {
		this.zjsqje = zjsqje;
	}
	public String getZjpfje() {
		return zjpfje;
	}
	public void setZjpfje(String zjpfje) {
		this.zjpfje = zjpfje;
	}
	public String getBz() {
		return bz;
	}
	public void setBz(String bz) {
		this.bz = bz;
	}
	public String getHjdsznmzrc() {
		return hjdsznmzrc;
	}
	public void setHjdsznmzrc(String hjdsznmzrc) {
		this.hjdsznmzrc = hjdsznmzrc;
	}
	public String getHjdsznmzsqje() {
		return hjdsznmzsqje;
	}
	public void setHjdsznmzsqje(String hjdsznmzsqje) {
		this.hjdsznmzsqje = hjdsznmzsqje;
	}
	public String getHjdsznmzpfje() {
		return hjdsznmzpfje;
	}
	public void setHjdsznmzpfje(String hjdsznmzpfje) {
		this.hjdsznmzpfje = hjdsznmzpfje;
	}
	public String getHjdsznzyrc() {
		return hjdsznzyrc;
	}
	public void setHjdsznzyrc(String hjdsznzyrc) {
		this.hjdsznzyrc = hjdsznzyrc;
	}
	public String getHjdsznzysqje() {
		return hjdsznzysqje;
	}
	public void setHjdsznzysqje(String hjdsznzysqje) {
		this.hjdsznzysqje = hjdsznzysqje;
	}
	public String getHjdsznzypfje() {
		return hjdsznzypfje;
	}
	public void setHjdsznzypfje(String hjdsznzypfje) {
		this.hjdsznzypfje = hjdsznzypfje;
	}
	public String getHjgygxmzrc() {
		return hjgygxmzrc;
	}
	public void setHjgygxmzrc(String hjgygxmzrc) {
		this.hjgygxmzrc = hjgygxmzrc;
	}
	public String getHjgygxmzsqje() {
		return hjgygxmzsqje;
	}
	public void setHjgygxmzsqje(String hjgygxmzsqje) {
		this.hjgygxmzsqje = hjgygxmzsqje;
	}
	public String getHjgygxmzpfje() {
		return hjgygxmzpfje;
	}
	public void setHjgygxmzpfje(String hjgygxmzpfje) {
		this.hjgygxmzpfje = hjgygxmzpfje;
	}
	public String getHjgygxzyrc() {
		return hjgygxzyrc;
	}
	public void setHjgygxzyrc(String hjgygxzyrc) {
		this.hjgygxzyrc = hjgygxzyrc;
	}
	public String getHjgygxzysqje() {
		return hjgygxzysqje;
	}
	public void setHjgygxzysqje(String hjgygxzysqje) {
		this.hjgygxzysqje = hjgygxzysqje;
	}
	public String getHjgygxzypfje() {
		return hjgygxzypfje;
	}
	public void setHjgygxzypfje(String hjgygxzypfje) {
		this.hjgygxzypfje = hjgygxzypfje;
	}
	public String getHjygywmzrc() {
		return hjygywmzrc;
	}
	public void setHjygywmzrc(String hjygywmzrc) {
		this.hjygywmzrc = hjygywmzrc;
	}
	public String getHjygywmzsqje() {
		return hjygywmzsqje;
	}
	public void setHjygywmzsqje(String hjygywmzsqje) {
		this.hjygywmzsqje = hjygywmzsqje;
	}
	public String getHjygywmzpfje() {
		return hjygywmzpfje;
	}
	public void setHjygywmzpfje(String hjygywmzpfje) {
		this.hjygywmzpfje = hjygywmzpfje;
	}
	public String getHjygywzyrc() {
		return hjygywzyrc;
	}
	public void setHjygywzyrc(String hjygywzyrc) {
		this.hjygywzyrc = hjygywzyrc;
	}
	public String getHjygywzysqje() {
		return hjygywzysqje;
	}
	public void setHjygywzysqje(String hjygywzysqje) {
		this.hjygywzysqje = hjygywzysqje;
	}
	public String getHjygywzypfje() {
		return hjygywzypfje;
	}
	public void setHjygywzypfje(String hjygywzypfje) {
		this.hjygywzypfje = hjygywzypfje;
	}
	public String getHjygywshrc() {
		return hjygywshrc;
	}
	public void setHjygywshrc(String hjygywshrc) {
		this.hjygywshrc = hjygywshrc;
	}
	public String getHjygywshsqje() {
		return hjygywshsqje;
	}
	public void setHjygywshsqje(String hjygywshsqje) {
		this.hjygywshsqje = hjygywshsqje;
	}
	public String getHjygywshpfje() {
		return hjygywshpfje;
	}
	public void setHjygywshpfje(String hjygywshpfje) {
		this.hjygywshpfje = hjygywshpfje;
	}
	public String getHjzjrc() {
		return hjzjrc;
	}
	public void setHjzjrc(String hjzjrc) {
		this.hjzjrc = hjzjrc;
	}
	public String getHjzjsqje() {
		return hjzjsqje;
	}
	public void setHjzjsqje(String hjzjsqje) {
		this.hjzjsqje = hjzjsqje;
	}
	public String getHjzjpfje() {
		return hjzjpfje;
	}
	public void setHjzjpfje(String hjzjpfje) {
		this.hjzjpfje = hjzjpfje;
	}
	public String getInsu_org_nm_ab() {
		return insu_org_nm_ab;
	}
	public void setInsu_org_nm_ab1(String insu_org_nm_ab) {
		this.insu_org_nm_ab = insu_org_nm_ab;
	}

	public String getRczj() {
		return rczj;
	}

	public String getSqjezj() {
		return sqjezj;
	}

	public String getPfjezj() {
		return pfjezj;
	}

	public void setRczj(String rczj) {
		this.rczj = rczj;
	}

	public void setSqjezj(String sqjezj) {
		this.sqjezj = sqjezj;
	}

	public void setPfjezj(String pfjezj) {
		this.pfjezj = pfjezj;
	}

	public String getPerson_level() {
		return person_level;
	}

	public String getQuery_condition() {
		return query_condition;
	}

	public void setPerson_level(String person_level) {
		this.person_level = person_level;
	}

	public void setQuery_condition(String query_condition) {
		this.query_condition = query_condition;
	}

	public long getUserid() {
		return userid;
	}

	public void setUserid(long userid) {
		this.userid = userid;
	}
	

}	
