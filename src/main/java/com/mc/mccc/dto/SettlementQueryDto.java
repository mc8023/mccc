package com.mc.mccc.dto;

public class SettlementQueryDto {
	
	private String person_level;//查询级别
	
	private String query_condition;//查询条件
	
    private String caseId;
    
    private String insuId;
    
    private String settlementStatus;
    
    private String insuName;
    
    private String idCard;
    
    private String miCardId;
    
    private String location;
    
    private String medYear;
    
    private String beginClinDate;
    
    private String endClinDate;
    
    private String beginReceDate;
    
    private String endReceDate;
    
    private String beginCloseCaseDate;
    
    private String endCloseCaseDate;
    
    private String hospitalName;
    
    private String jmhs;
    
    public String getPerson_level() {
		return person_level;
	}

	public void setPerson_level(String person_level) {
		this.person_level = person_level;
	}

	public String getQuery_condition() {
		return query_condition;
	}

	public void setQuery_condition(String query_condition) {
		this.query_condition = query_condition;
	}

	public String getCaseId() {
        return caseId;
    }

    public void setCaseId(String caseId) {
        this.caseId = caseId;
    }

    public String getSettlementStatus() {
        return settlementStatus;
    }

    public void setSettlementStatus(String settlementStatus) {
        this.settlementStatus = settlementStatus;
    }

    public String getInsuName() {
        return insuName;
    }

    public void setInsuName(String insuName) {
        this.insuName = insuName;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getMiCardId() {
        return miCardId;
    }

    public void setMiCardId(String miCardId) {
        this.miCardId = miCardId;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }


    public String getMedYear() {
        return medYear;
    }

    public void setMedYear(String medYear) {
        this.medYear = medYear;
    }

    public String getBeginClinDate() {
        return beginClinDate;
    }

    public void setBeginClinDate(String beginClinDate) {
        this.beginClinDate = beginClinDate;
    }

    public String getEndClinDate() {
        return endClinDate;
    }

    public void setEndClinDate(String endClinDate) {
        this.endClinDate = endClinDate;
    }

    
    public String getBeginReceDate() {
        return beginReceDate;
    }

    public void setBeginReceDate(String beginReceDate) {
        this.beginReceDate = beginReceDate;
    }

    public String getEndReceDate() {
        return endReceDate;
    }

    public void setEndReceDate(String endReceDate) {
        this.endReceDate = endReceDate;
    }

    public String getBeginCloseCaseDate() {
        return beginCloseCaseDate;
    }

    public void setBeginCloseCaseDate(String beginCloseCaseDate) {
        this.beginCloseCaseDate = beginCloseCaseDate;
    }

    public String getEndCloseCaseDate() {
        return endCloseCaseDate;
    }

    public void setEndCloseCaseDate(String endCloseCaseDate) {
        this.endCloseCaseDate = endCloseCaseDate;
    }

    public String getHospitalName() {
        return hospitalName;
    }

    public void setHospitalName(String hospitalName) {
        this.hospitalName = hospitalName;
    }

	public String getJmhs() {
		return jmhs;
	}

	public void setJmhs(String jmhs) {
		this.jmhs = jmhs;
	}

	public String getInsuId() {
		return insuId;
	}

	public void setInsuId(String insuId) {
		this.insuId = insuId;
	}
    

}
