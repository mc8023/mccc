package com.mc.mccc.dto;

import java.util.List;

public class LpxxDto {
	
	private List<String> insuid_medyear;//被保人ID和医疗年度的拼接查询条件
	private String case_id;//案件号
	private String case_status;//案件状态
	private String happen_date;//申请时间	
	private Double bill_total;//发票总金额
	private Double cut_pay;//核减总金额
	private Double outp_comp;//门诊赔付
	private Double hosp_claims;//住院赔付
	private Double pfze;//赔付总额
	private String refu_flag;//是否拒付
	private String hosp_id;//机构代码
	private String clin_id;//就诊流水号
	private String tbrxm;//投保人姓名
	private String bbrxm;//被保人姓名
	private String med_year;//医疗年度
	private String bbrsfz;//被保人身份证号
	private String ygbh;//员工编码
	private String dwmc;//单位名称
	private String insu_type;//投保类型
	private String policy_date;//投保周期
	private String lplx;//理赔类型
	
	private String person_level;//查询级别
	
	private String query_condition;//查询条件
	
	private long userid;
	
	
	public String getPerson_level() {
		return person_level;
	}
	public void setPerson_level(String person_level) {
		this.person_level = person_level;
	}
	public String getQuery_condition() {
		return query_condition;
	}
	public void setQuery_condition(String query_condition) {
		this.query_condition = query_condition;
	}
	public long getUserid() {
		return userid;
	}
	public void setUserid(long userid) {
		this.userid = userid;
	}
	public String getLplx() {
		return lplx;
	}
	public void setLplx(String lplx) {
		this.lplx = lplx;
	}
	public String getCase_id() {
		return case_id;
	}
	public void setCase_id(String case_id) {
		this.case_id = case_id;
	}
	public String getCase_status() {
		return case_status;
	}
	public void setCase_status(String case_status) {
		this.case_status = case_status;
	}
	public String getHappen_date() {
		return happen_date;
	}
	public void setHappen_date(String happen_date) {
		this.happen_date = happen_date;
	}
	public Double getBill_total() {
		return bill_total;
	}
	public void setBill_total(Double bill_total) {
		this.bill_total = bill_total;
	}
	public Double getCut_pay() {
		return cut_pay;
	}
	public void setCut_pay(Double cut_pay) {
		this.cut_pay = cut_pay;
	}
	public Double getOutp_comp() {
		return outp_comp;
	}
	public void setOutp_comp(Double outp_comp) {
		this.outp_comp = outp_comp;
	}
	public Double getHosp_claims() {
		return hosp_claims;
	}
	public void setHosp_claims(Double hosp_claims) {
		this.hosp_claims = hosp_claims;
	}
	public Double getPfze() {
		return pfze;
	}
	public void setPfze(Double pfze) {
		this.pfze = pfze;
	}
	public String getRefu_flag() {
		return refu_flag;
	}
	public void setRefu_flag(String refu_flag) {
		this.refu_flag = refu_flag;
	}
	public String getHosp_id() {
		return hosp_id;
	}
	public void setHosp_id(String hosp_id) {
		this.hosp_id = hosp_id;
	}
	public String getClin_id() {
		return clin_id;
	}
	public void setClin_id(String clin_id) {
		this.clin_id = clin_id;
	}
	public List<String> getInsuid_medyear() {
		return insuid_medyear;
	}
	public void setInsuid_medyear(List<String> insuid_medyear) {
		this.insuid_medyear = insuid_medyear;
	}
	public String getTbrxm() {
		return tbrxm;
	}
	public void setTbrxm(String tbrxm) {
		this.tbrxm = tbrxm;
	}
	public String getBbrxm() {
		return bbrxm;
	}
	public void setBbrxm(String bbrxm) {
		this.bbrxm = bbrxm;
	}
	public String getMed_year() {
		return med_year;
	}
	public void setMed_year(String med_year) {
		this.med_year = med_year;
	}
	public String getBbrsfz() {
		return bbrsfz;
	}
	public void setBbrsfz(String bbrsfz) {
		this.bbrsfz = bbrsfz;
	}
	public String getYgbh() {
		return ygbh;
	}
	public void setYgbh(String ygbh) {
		this.ygbh = ygbh;
	}
	public String getDwmc() {
		return dwmc;
	}
	public void setDwmc(String dwmc) {
		this.dwmc = dwmc;
	}
	public String getInsu_type() {
		return insu_type;
	}
	public void setInsu_type(String insu_type) {
		this.insu_type = insu_type;
	}
	public String getPolicy_date() {
		return policy_date;
	}
	public void setPolicy_date(String policy_date) {
		this.policy_date = policy_date;
	}
	
}
