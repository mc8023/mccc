package com.mc.mccc.dto;

/**
 * 
 * <p>@describe: 年度理赔台账 查询参数 </P>
 * <p>@title: NianDuLiPeiDto.java </P>
 * <p>@package: com.mc.mccc.dto </P>
 * <p>@date: 2017年5月17日 下午2:39:38  </P>
 * <p>@author: wangaogao  </P> 
 * <p>@Copyright: (c) 2017, 上海金仕达卫宁软件科技有限公司  </P>
 */
public class NianDuLiPeiDto {
	/**年度*/
	private String nd;
	
	/**板块*/
	private String bk;

	/**理赔类型
	 * @return
	 */
	private String lplx;
	public String getLplx() {
		return lplx;
	}

	public void setLplx(String lplx) {
		this.lplx = lplx;
	}

	public String getNd() {
		return nd;
	}

	public void setNd(String nd) {
		this.nd = nd;
	}

	public String getBk() {
		return bk;
	}

	public void setBk(String bk) {
		this.bk = bk;
	}
	
}
