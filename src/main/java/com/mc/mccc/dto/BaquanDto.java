package com.mc.mccc.dto;

public class BaquanDto {
	private String chan_id;				//变动流水号
	private String insuName;			//
	private String idCard;				//
	private String status;				//
	private String xh;					//
	private String nf;					//
	private String insuid;				//
	private String tblx;				//
	private String bdlx;				//
	private String insu_nm_before;		//
	private String ygid_before;			//
	private String ygnm_before;			//
	private String gzdw_before;			//
	private String bbrcsny_before;		//
	private String bbrsfzh_before;		//
	private String tbkssj_before;		//
	private String tbjssj_before;		//
	private String insu_nm_new;			//
	private String ygid_new;			//
	private String ygnm_new;			//
	private String gzdw_new;			//
	private String bbrcsny_new;
	private String bbrsfzh_new;
	private String tbkssj_new;
	private String tbjssj_new;
	private String sex_new;
	private String qsgx_new;
	private String tbsd_new;
	private String jzd_new;
	private String zjys;
	private String lrsj;
	private String lrry;
	private String shsj;
	private String shry;
	private String bz;
	private String bt;
	private String dctj;				//拼接导出条件（insuid nf）
	private String tblx1;
	private String caseId;
	private String bbrxm;
	private String tbrxm;
	private String bbrsfz;
	private String ygbh;
	private String dwmc;
	private String jmhs;
    
	private long userid;
	private String person_level;//查询级别
	
	private String query_condition;//查询条件
	
	public String getChan_id() {
		return chan_id;
	}
	public void setChan_id(String chan_id) {
		this.chan_id = chan_id;
	}
	public String getInsuName() {
		return insuName;
	}
	public void setInsuName(String insuName) {
		this.insuName = insuName;
	}
	public String getIdCard() {
		return idCard;
	}
	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getXh() {
		return xh;
	}
	public void setXh(String xh) {
		this.xh = xh;
	}
	public String getNf() {
		return nf;
	}
	public void setNf(String nf) {
		this.nf = nf;
	}
	public String getTblx() {
		return tblx;
	}
	public void setTblx(String tblx) {
		this.tblx = tblx;
	}
	public String getBdlx() {
		return bdlx;
	}
	public void setBdlx(String bdlx) {
		this.bdlx = bdlx;
	}
	public String getInsu_nm_before() {
		return insu_nm_before;
	}
	public void setInsu_nm_before(String insu_nm_before) {
		this.insu_nm_before = insu_nm_before;
	}
	public String getYgid_before() {
		return ygid_before;
	}
	public void setYgid_before(String ygid_before) {
		this.ygid_before = ygid_before;
	}
	public String getYgnm_before() {
		return ygnm_before;
	}
	public void setYgnm_before(String ygnm_before) {
		this.ygnm_before = ygnm_before;
	}
	public String getGzdw_before() {
		return gzdw_before;
	}
	public void setGzdw_before(String gzdw_before) {
		this.gzdw_before = gzdw_before;
	}
	public String getBbrcsny_before() {
		return bbrcsny_before;
	}
	public void setBbrcsny_before(String bbrcsny_before) {
		this.bbrcsny_before = bbrcsny_before;
	}
	public String getBbrsfzh_before() {
		return bbrsfzh_before;
	}
	public void setBbrsfzh_before(String bbrsfzh_before) {
		this.bbrsfzh_before = bbrsfzh_before;
	}
	public String getTbkssj_before() {
		return tbkssj_before;
	}
	public void setTbkssj_before(String tbkssj_before) {
		this.tbkssj_before = tbkssj_before;
	}
	public String getTbjssj_before() {
		return tbjssj_before;
	}
	public void setTbjssj_before(String tbjssj_before) {
		this.tbjssj_before = tbjssj_before;
	}
	public String getInsu_nm_new() {
		return insu_nm_new;
	}
	public void setInsu_nm_new(String insu_nm_new) {
		this.insu_nm_new = insu_nm_new;
	}
	public String getYgid_new() {
		return ygid_new;
	}
	public void setYgid_new(String ygid_new) {
		this.ygid_new = ygid_new;
	}
	public String getYgnm_new() {
		return ygnm_new;
	}
	public void setYgnm_new(String ygnm_new) {
		this.ygnm_new = ygnm_new;
	}
	public String getGzdw_new() {
		return gzdw_new;
	}
	public void setGzdw_new(String gzdw_new) {
		this.gzdw_new = gzdw_new;
	}
	public String getBbrcsny_new() {
		return bbrcsny_new;
	}
	public void setBbrcsny_new(String bbrcsny_new) {
		this.bbrcsny_new = bbrcsny_new;
	}
	public String getBbrsfzh_new() {
		return bbrsfzh_new;
	}
	public void setBbrsfzh_new(String bbrsfzh_new) {
		this.bbrsfzh_new = bbrsfzh_new;
	}
	public String getTbkssj_new() {
		return tbkssj_new;
	}
	public void setTbkssj_new(String tbkssj_new) {
		this.tbkssj_new = tbkssj_new;
	}
	public String getTbjssj_new() {
		return tbjssj_new;
	}
	public void setTbjssj_new(String tbjssj_new) {
		this.tbjssj_new = tbjssj_new;
	}
	public String getSex_new() {
		return sex_new;
	}
	public void setSex_new(String sex_new) {
		this.sex_new = sex_new;
	}
	public String getQsgx_new() {
		return qsgx_new;
	}
	public void setQsgx_new(String qsgx_new) {
		this.qsgx_new = qsgx_new;
	}
	public String getTbsd_new() {
		return tbsd_new;
	}
	public void setTbsd_new(String tbsd_new) {
		this.tbsd_new = tbsd_new;
	}
	public String getJzd_new() {
		return jzd_new;
	}
	public void setJzd_new(String jzd_new) {
		this.jzd_new = jzd_new;
	}
	public String getZjys() {
		return zjys;
	}
	public void setZjys(String zjys) {
		this.zjys = zjys;
	}
	public String getLrsj() {
		return lrsj;
	}
	public void setLrsj(String lrsj) {
		this.lrsj = lrsj;
	}
	public String getLrry() {
		return lrry;
	}
	public void setLrry(String lrry) {
		this.lrry = lrry;
	}
	public String getShsj() {
		return shsj;
	}
	public void setShsj(String shsj) {
		this.shsj = shsj;
	}
	public String getShry() {
		return shry;
	}
	public void setShry(String shry) {
		this.shry = shry;
	}
	public String getBz() {
		return bz;
	}
	public void setBz(String bz) {
		this.bz = bz;
	}
	public String getInsuid() {
		return insuid;
	}
	public void setInsuid(String insuid) {
		this.insuid = insuid;
	}
	public String getBt() {
		return bt;
	}
	public void setBt(String bt) {
		this.bt = bt;
	}
	public String getDctj() {
		return dctj;
	}
	public void setDctj(String dctj) {
		this.dctj = dctj;
	}
	public String getTblx1() {
		return tblx1;
	}
	public void setTblx1(String tblx1) {
		this.tblx1 = tblx1;
	}
	public String getCaseId() {
		return caseId;
	}
	public void setCaseId(String caseId) {
		this.caseId = caseId;
	}
	public String getBbrxm() {
		return bbrxm;
	}
	public void setBbrxm(String bbrxm) {
		this.bbrxm = bbrxm;
	}
	public String getTbrxm() {
		return tbrxm;
	}
	public void setTbrxm(String tbrxm) {
		this.tbrxm = tbrxm;
	}
	public String getBbrsfz() {
		return bbrsfz;
	}
	public void setBbrsfz(String bbrsfz) {
		this.bbrsfz = bbrsfz;
	}
	public String getYgbh() {
		return ygbh;
	}
	public String getDwmc() {
		return dwmc;
	}
	public void setYgbh(String ygbh) {
		this.ygbh = ygbh;
	}
	public void setDwmc(String dwmc) {
		this.dwmc = dwmc;
	}
	public String getJmhs() {
		return jmhs;
	}
	public void setJmhs(String jmhs) {
		this.jmhs = jmhs;
	}
	public String getPerson_level() {
		return person_level;
	}
	public void setPerson_level(String person_level) {
		this.person_level = person_level;
	}
	public String getQuery_condition() {
		return query_condition;
	}
	public void setQuery_condition(String query_condition) {
		this.query_condition = query_condition;
	}
	public long getUserid() {
		return userid;
	}
	public void setUserid(long userid) {
		this.userid = userid;
	}

}
