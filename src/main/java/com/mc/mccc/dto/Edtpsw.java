package com.mc.mccc.dto;

public class Edtpsw {
	
	private String psw1;
	private String psw2;
	private String psw3;
	private Integer id;
	private String username; 
	
	public String getPsw1() {
		return psw1;
	}
	public String getPsw2() {
		return psw2;
	}
	public String getPsw3() {
		return psw3;
	}
	public void setPsw1(String psw1) {
		this.psw1 = psw1;
	}
	public void setPsw2(String psw2) {
		this.psw2 = psw2;
	}
	public void setPsw3(String psw3) {
		this.psw3 = psw3;
	}
	public Integer getId() {
		return id;
	}
	public String getUsername() {
		return username;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	
	
	

}
