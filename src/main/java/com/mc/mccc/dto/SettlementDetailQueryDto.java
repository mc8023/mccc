package com.mc.mccc.dto;

import java.io.Serializable;

import com.mc.mccc.comm.PageResult;

/**
 * 理赔明细
 * @author 唐修亮
 *
 */
public class SettlementDetailQueryDto implements Serializable{
	private static final long serialVersionUID = 8084847794595964204L;
	
	private PageResult pageResult;
	private String rownums;
	private String zjsqje;
	private String zjpfje;
	private String lplx;
	private String jfbs;
	private String area_id;

	private String item_value;			//属地值
	private String item_nm;				//属地名称
	private String itemnms;
	private String hjsqje;
	private String hjcomp_pay;

	private String insu_org_nm_ab;
	private String insu_org_id;
	private String bk;
	private String insu_type;
	private String third_pay;
	
	private String beginClinDate;		//开始日期
	private String endClinDate;		//开始日期
	private String areaid;				//属地
	private String pbk;					//板块
	private String case_id;				//索赔流水号
	private String insu_org_nm;			//单位
	private String poli_hold_nm;		//申请人
	private String insu_nm;				//被保险人id
	private String id_card;				//身份证号	
	private String insutype;			//人员类别
	private String hosp_nm;				//医院类别
	private String clinic_type;			//费用类别
	private String happen_date;			//出险日期
	private String rece_date;			//申请日期
	private String close_date;			//结案日期
	private String arrival_date;		//到账日期
	private String sqje;				//申请金额
	private String mp;					//免赔
	private String cut_pay; 			//扣减金额
	private String comp_pay;			//赔付金额
	private String comp_rate;			//赔付比例
	private String by;					//病因
	private String bz;					//备注
	//private String CLOSE_DATE;
	private String REMIT_DATE;
	private String effect_date;
	private String shcs;
	private String lscs;
	private String cssj;				
	private String jmhs;				//解密密匙
	private String zksxsz;				//时效设置
	private String bzxx;
	private long userid;
	
	private String person_level;//查询级别
	
	private String query_condition;//查询条件
	
	public String getArea_id() {
		return area_id;
	}
	public void setArea_id(String area_id) {
		this.area_id = area_id;
	}
	public String getLplx() {
		return lplx;
	}
	public void setLplx(String lplx) {
		this.lplx = lplx;
	}
	public String getZjsqje() {
		return zjsqje;
	}
	public void setZjsqje(String zjsqje) {
		this.zjsqje = zjsqje;
	}
	public String getZjpfje() {
		return zjpfje;
	}
	public void setZjpfje(String zjpfje) {
		this.zjpfje = zjpfje;
	}
	public String getRownums() {
		return rownums;
	}
	public void setRownum(String rownums) {
		this.rownums = rownums;
	}
	public PageResult getPageResult() {
		return pageResult;
	}
	public void setPageResult(PageResult pageResult) {
		this.pageResult = pageResult;
	}
	public String getHjsqje() {
		return hjsqje;
	}
	public void setHjsqje(String hjsqje) {
		this.hjsqje = hjsqje;
	}
	public String getHjcomp_pay() {
		return hjcomp_pay;
	}
	public void setHjcomp_pay(String hjcomp_pay) {
		this.hjcomp_pay = hjcomp_pay;
	}
	public String getItemnms() {
		return itemnms;
	}
	public void setItemnms(String itemnms) {
		this.itemnms = itemnms;
	}
	public String getShcs() {
		return shcs;
	}
	public void setShcs(String shcs) {
		this.shcs = shcs;
	}
	public String getLscs() {
		return lscs;
	}
	public void setLscs(String lscs) {
		this.lscs = lscs;
	}
	public String getCssj() {
		return cssj;
	}
	public void setCssj(String cssj) {
		this.cssj = cssj;
	}
	public String getEffect_date() {
		return effect_date;
	}
	public void setEffect_date(String effect_date) {
		this.effect_date = effect_date;
	}
	public String getREMIT_DATE() {
		return REMIT_DATE;
	}
	public void setREMIT_DATE(String rEMIT_DATE) {
		REMIT_DATE = rEMIT_DATE;
	}
/*	public String getCLOSE_DATE() {
		return CLOSE_DATE;
	}
	public void setCLOSE_DATE(String cLOSE_DATE) {
		CLOSE_DATE = cLOSE_DATE;
	}*/
	public String getThird_pay() {
		return third_pay;
	}
	public void setThird_pay(String third_pay) {
		this.third_pay = third_pay;
	}
	public String getBk() {
		return bk;
	}
	public void setBk(String bk) {
		this.bk = bk;
	}
	public String getInsu_type() {
		return insu_type;
	}
	public void setInsu_type(String insu_type) {
		this.insu_type = insu_type;
	}
	public String getInsu_org_id() {
		return insu_org_id;
	}
	public void setInsu_org_id(String insu_org_id) {
		this.insu_org_id = insu_org_id;
	}
	public String getInsu_org_nm_ab() {
		return insu_org_nm_ab;
	}
	public void setInsu_org_nm_ab(String insu_org_nm_ab) {
		this.insu_org_nm_ab = insu_org_nm_ab;
	}
	public String getItem_value() {
		return item_value;
	}
	public void setItem_value(String item_value) {
		this.item_value = item_value;
	}
	public String getItem_nm() {
		return item_nm;
	}
	public void setItem_nm(String item_nm) {
		this.item_nm = item_nm;
	}

	public String getBeginClinDate() {
		return beginClinDate;
	}
	public void setBeginClinDate(String beginClinDate) {
		this.beginClinDate = beginClinDate;
	}
	public String getAreaid() {
		return areaid;
	}
	public void setAreaid(String areaid) {
		this.areaid = areaid;
	}
	public String getPbk() {
		return pbk;
	}
	
	public void setPbk(String pbk) {
		this.pbk = pbk;
	}
	public String getCase_id() {
		return case_id;
	}
	public void setCase_id(String case_id) {
		this.case_id = case_id;
	}
	public String getInsu_org_nm() {
		return insu_org_nm;
	}
	public void setInsu_org_nm(String insu_org_nm) {
		this.insu_org_nm = insu_org_nm;
	}
	public String getPoli_hold_nm() {
		return poli_hold_nm;
	}
	public void setPoli_hold_nm(String poli_hold_nm) {
		this.poli_hold_nm = poli_hold_nm;
	}
	public String getInsu_nm() {
		return insu_nm;
	}
	public void setInsu_nm(String insu_nm) {
		this.insu_nm = insu_nm;
	}
	public String getId_card() {
		return id_card;
	}
	public void setId_card(String id_card) {
		this.id_card = id_card;
	}
	public String getInsutype() {
		return insutype;
	}
	public void setInsutype(String insutype) {
		this.insutype = insutype;
	}
	public String getHosp_nm() {
		return hosp_nm;
	}
	public void setHosp_nm(String hosp_nm) {
		this.hosp_nm = hosp_nm;
	}
	public String getClinic_type() {
		return clinic_type;
	}
	public void setClinic_type(String clinic_type) {
		this.clinic_type = clinic_type;
	}
	public String getHappen_date() {
		return happen_date;
	}
	public void setHappen_date(String happen_date) {
		this.happen_date = happen_date;
	}
	public String getRece_date() {
		return rece_date;
	}
	public void setRece_date(String rece_date) {
		this.rece_date = rece_date;
	}
	public String getClose_date() {
		return close_date;
	}
	public void setClose_date(String close_date) {
		this.close_date = close_date;
	}
	public String getArrival_date() {
		return arrival_date;
	}
	public void setArrival_date(String arrival_date) {
		this.arrival_date = arrival_date;
	}
	public String getSqje() {
		return sqje;
	}
	public void setSqje(String sqje) {
		this.sqje = sqje;
	}
	public String getMp() {
		return mp;
	}
	public void setMp(String mp) {
		this.mp = mp;
	}
	public String getCut_pay() {
		return cut_pay;
	}
	public void setCut_pay(String cut_pay) {
		this.cut_pay = cut_pay;
	}
	public String getComp_pay() {
		return comp_pay;
	}
	public void setComp_pay(String comp_pay) {
		this.comp_pay = comp_pay;
	}
	public String getComp_rate() {
		return comp_rate;
	}
	public void setComp_rate(String comp_rate) {
		this.comp_rate = comp_rate;
	}
	public String getBy() {
		return by;
	}
	public void setBy(String by) {
		this.by = by;
	}
	public String getBz() {
		return bz;
	}
	public void setBz(String bz) {
		this.bz = bz;
	}
	public String getEndClinDate() {
		return endClinDate;
	}
	public void setEndClinDate(String endClinDate) {
		this.endClinDate = endClinDate;
	}
	public String getPerson_level() {
		return person_level;
	}
	public String getQuery_condition() {
		return query_condition;
	}
	public void setRownums(String rownums) {
		this.rownums = rownums;
	}
	public void setPerson_level(String person_level) {
		this.person_level = person_level;
	}
	public void setQuery_condition(String query_condition) {
		this.query_condition = query_condition;
	}
	public String getJmhs() {
		return jmhs;
	}
	public void setJmhs(String jmhs) {
		this.jmhs = jmhs;
	}
	public String getZksxsz() {
		return zksxsz;
	}
	public void setZksxsz(String zksxsz) {
		this.zksxsz = zksxsz;
	}
	public String getJfbs() {
		return jfbs;
	}
	public void setJfbs(String jfbs) {
		this.jfbs = jfbs;
	}
	public String getBzxx() {
		return bzxx;
	}
	public void setBzxx(String bzxx) {
		this.bzxx = bzxx;
	}
	public long getUserid() {
		return userid;
	}
	public void setUserid(long userid) {
		this.userid = userid;
	}
	
	
	
}	
