package com.mc.mccc.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.draw.LineSeparator;



public class CreateClaimPdfUtil {
    
    private static Logger logger = Logger.getLogger(CreateClaimPdfUtil.class);
    
    private static Font font14BOLD;
    private static Font font10BOLD;
    private static Font font8BOLD;
    private static Font font8NORMAL;
    private static Font font5BOLD;
    static{
        try {
            font14BOLD = getFont14BOLD();
            font10BOLD = getFont10NORMAL();
            font8BOLD = getFont8BOLD();
            font8NORMAL = getFont8NORMAL();
            font5BOLD = getFont5NORMAL();
        }
        catch (Exception e) {
            logger.error("", e);
        }
    }
    
    public static File createClaimPdf(List<Map<String, Object>> tbInfo, List<Map<String, Object>> zymxInfoDatas, List<Map<String, Object>> mzmxInfoDatas ,List<Map<String, Object>> hjInfoDatas ,List<Map<String, Object>> lpgyInfoDatas,List<Map<String, Object>> zyhjInfoDatas){
        Document document = new Document(PageSize.A4); 
        String tempPath = getClassPath()+"\\temp";
        File file = new File(tempPath);
        if(!file.exists()){
            file.mkdirs();
        }
        file = new File(tempPath+File.separator+UUID.randomUUID().toString()+".pdf");
        try {  
            //写入文本到文件中  
            PdfWriter.getInstance(document, new FileOutputStream(file));  
            //打开文本  
            document.open();  
            //定义段落  
            setHeaderTitle(document);
            //横线
            setLineSeparator(document);
          //  getSettlementInfoTables(settlementInfo, document);
            //换行
            wrapHandle(document);
            //发票详情
            //getBillInfoTables(billInfoDtos , document);
            //换行
            // 投保信息
            document.add(getParagraph("投保信息", Element.ALIGN_CENTER, font10BOLD, 5));
            getIllegalInfoTables(tbInfo, document);
            wrapHandle(document);
            // 本次理赔概览
            document.add(getParagraph("本次理赔概览", Element.ALIGN_CENTER, font10BOLD, 5));
            getLipeiInfoInfoTables(lpgyInfoDatas, document);
            wrapHandle(document);
            // 住院理赔明细
            document.add(getParagraph("住院理赔明细", Element.ALIGN_LEFT, font10BOLD, 5));
           getZyLipeiInfoInfoTables(zymxInfoDatas, document);
            wrapHandle(document);
            
            // 住院合计
            document.add(getParagraph("住院合计信息", Element.ALIGN_LEFT, font10BOLD, 5));
            getZyHjInfoInfoTables(zyhjInfoDatas, document);
            wrapHandle(document);
            
            
            // 门诊理赔明细
            document.add(getParagraph("门诊理赔明细", Element.ALIGN_LEFT, font10BOLD, 5));
            getMzLipeiInfoInfoTables(mzmxInfoDatas, document);
            wrapHandle(document);
            // 合计
            document.add(getParagraph("合计信息", Element.ALIGN_LEFT, font10BOLD, 5));
            getHjInfoInfoTables(hjInfoDatas, document);
            
            wrapHandle(document);
            // 温馨提示
            document.add(getParagraph("备注：理赔计算方式详见服务手册", Element.ALIGN_LEFT, font10BOLD, 5));
           
            
            
            document.close();  
          } catch (Exception e) {  
            e.printStackTrace();  
          } 
        
        return file;
    }
    
    
    private static void getSettlementInfoTables(Map<String, Object> settlementInfo, Document document) throws DocumentException, IOException{
        String [] settlementInfoArr1 = new String[]{"申请险种名称","案件收集日期","累 计 免 赔 额"};
        String [] settlementInfoArr2 = new String[]{"申 请 人 姓 名","理赔结案日期","累计赔付金额"};
        String [] settlementInfoArr3 = new String[]{"被保险人姓名","理赔划账日期"};
        String [] settlementInfoArr4 = new String[]{"参保单位名称","违规警告"};
        document.add(getSettlementInfoTable(settlementInfo, settlementInfoArr1, 3, new int[]{45,30,25}));
        document.add(getSettlementInfoTable(settlementInfo, settlementInfoArr2, 3, new int[]{45,30,25}));
        document.add(getSettlementInfoTable(settlementInfo, settlementInfoArr3, 2, new int[]{45,55}));
        document.add(getSettlementInfoTable(settlementInfo, settlementInfoArr4, 2, new int[]{45,55}));
    }
    
   
    
    /*private static void getBillInfoTables(List<PatrolCourtDTO> billInfoDtos, Document document) throws DocumentException, IOException{
        //放头部cell
        int num = 10;
        int [] width = new int[]{8,8,20,6,10,8,10,10,10,10};
        document.add(getBillInfoHeader(num, width));
        //放发票详情
        document.add(getBillInfoBody(billInfoDtos, num, width));
        
    }*/
    
    /**
     * 投保信息
     * */
    private static void getIllegalInfoTables(List<Map<String, Object>> tbInfo, Document document) throws DocumentException, IOException{
        //放头部cell
        int num = 7;
        int [] width = new int[]{15,18,18,18,28,15,23};
        document.add(getIllegalInfoHeader(num, width));
        //放投保信息详情
        document.add(getTbInfoBody(tbInfo, num, width));
        
    }
    
    
    /**
     * 投保信息结果集
     * */
    private static PdfPTable getTbInfoBody(List<Map<String, Object>> tbInfo, int num, int [] width) throws DocumentException{
        PdfPTable table = new PdfPTable(num);
        table.setWidths(width); 
        boolean isSumBill = false;
        for(int i = 0; i<tbInfo.size();i++){
            //需要数据转化
           // table.addCell(getPdfCell(""+billInfoDto.getClinDate())); 
            if(i==tbInfo.size()-1){
                isSumBill = true;
            }
           
            table.addCell(getPdfCell(""+tbInfo.get(i).get("INSU_NM"), isSumBill));
            table.addCell(getPdfCell(""+tbInfo.get(i).get("ID_CARD"), isSumBill));
            table.addCell(getPdfCell(""+tbInfo.get(i).get("POLI_HOLD_NM"), isSumBill));
            table.addCell(getPdfCell(""+tbInfo.get(i).get("POLI_HOLD_ID"), isSumBill));
            table.addCell(getPdfCell(""+tbInfo.get(i).get("INSU_ORG_NM"), isSumBill));
            table.addCell(getPdfCell(""+tbInfo.get(i).get("INSU_TYPE"), isSumBill));
            table.addCell(getPdfCell(""+tbInfo.get(i).get("POLICY_BEGIN_DT")+"~"+tbInfo.get(i).get("POLICY_END_DT"), isSumBill));
           
        }
        table.setWidthPercentage(100);
        table.setHorizontalAlignment(Element.ALIGN_LEFT);
        return table;
    }
    
    
    /**
     * 住院合计信息
     * */
    private static void getZyHjInfoInfoTables(List<Map<String, Object>> zyhjInfoDatas, Document document) throws DocumentException, IOException{
        //放头部cell
        int num = 4;
        int [] width = new int[]{15,20,15,15};
        document.add(getZyHjInfoHeader(num, width));
        //放发票详情
        document.add(getZyHjInfoBody(zyhjInfoDatas, num, width));
        
    }
    
   
    
    /**
     * 住院理赔明细结果集
     * */
    private static PdfPTable getZymxInfoBody(List<Map<String, Object>> zymxInfoDatas, int num, int [] width) throws DocumentException{
        PdfPTable table = new PdfPTable(num);
        table.setWidths(width); 
        boolean isSumBill = false;
        for(int i = 0; i<zymxInfoDatas.size();i++){
            //需要数据转化
           // table.addCell(getPdfCell(""+billInfoDto.getClinDate())); 
            if(i==zymxInfoDatas.size()-1){
                isSumBill = true;
            }
           
            if(zymxInfoDatas.get(i).get("BILL_CODE")!=null){
            	 table.addCell(getPdfCell(""+zymxInfoDatas.get(i).get("BILL_CODE"), isSumBill));
            }else{
            	 table.addCell(getPdfCell(""+"", isSumBill));
            }
            
            
            if(zymxInfoDatas.get(i).get("HOSP_NM")!=null){
            	  table.addCell(getPdfCell(""+zymxInfoDatas.get(i).get("HOSP_NM"), isSumBill));
           }else{
           	 table.addCell(getPdfCell(""+"", isSumBill));
           }
            if(zymxInfoDatas.get(i).get("IN_HOSP_DATE")!=null){
            	 table.addCell(getPdfCell(""+zymxInfoDatas.get(i).get("IN_HOSP_DATE"), isSumBill));
            }else{
         	 table.addCell(getPdfCell(""+"", isSumBill));
            }
            if(zymxInfoDatas.get(i).get("OUT_HOSP_DATE")!=null){
            	  table.addCell(getPdfCell(""+zymxInfoDatas.get(i).get("OUT_HOSP_DATE"), isSumBill));
           }else{
        	 table.addCell(getPdfCell(""+"", isSumBill));
           }
            if(zymxInfoDatas.get(i).get("BILL_TOTAL")!=null){
            	  table.addCell(getPdfCell(""+zymxInfoDatas.get(i).get("BILL_TOTAL"), isSumBill));
            }else{
             table.addCell(getPdfCell(""+"0", isSumBill));
           }
          
          
            table.addCell(getPdfCell(""+zymxInfoDatas.get(i).get("THIRD_PAY"), isSumBill));
            table.addCell(getPdfCell(""+zymxInfoDatas.get(i).get("CUT_PAY"), isSumBill));
            
            if(zymxInfoDatas.get(i).get("HOSP_CLAIMS")!=null){
            	  table.addCell(getPdfCell(""+zymxInfoDatas.get(i).get("HOSP_CLAIMS"), isSumBill));
            }else{
            	table.addCell(getPdfCell(""+"0", isSumBill));
            }
          
            
            
            
            if(null==zymxInfoDatas.get(i).get("CUT_PAY") || "0".equals(zymxInfoDatas.get(i).get("CUT_PAY").toString())){
           	 table.addCell(getPdfCell(""+"", isSumBill));
           }else{
           	 table.addCell(getPdfCell(""+zymxInfoDatas.get(i).get("ITEM_NM"), isSumBill));
           }
            if(null!=zymxInfoDatas.get(i).get("REFU_FLAG") && "1".equals(zymxInfoDatas.get(i).get("REFU_FLAG").toString())){
              	 table.addCell(getPdfCell(""+"拒付", isSumBill));
              }else{
              	 table.addCell(getPdfCell(""+"", isSumBill));
              }
        }
        table.setWidthPercentage(100);
        table.setHorizontalAlignment(Element.ALIGN_LEFT);
        return table;
    }
    
    /**
     * 住院合计信息结果集
     * */
    private static PdfPTable getZyHjInfoBody(List<Map<String, Object>> zyhjInfoDatas , int num, int [] width) throws DocumentException{
        PdfPTable table = new PdfPTable(num);
        table.setWidths(width); 
        boolean isSumBill = false;
        for(int i = 0; i<zyhjInfoDatas.size();i++){
            //需要数据转化
           // table.addCell(getPdfCell(""+billInfoDto.getClinDate())); 
            if(i==zyhjInfoDatas.size()-1){
                isSumBill = true;
            }
           
            if("".equals(zyhjInfoDatas.get(i).get("BILL_TOTAL")) || null==zyhjInfoDatas.get(i).get("BILL_TOTAL")){
            	table.addCell(getPdfCell(""+0, isSumBill));
            }else{
            	table.addCell(getPdfCell(""+zyhjInfoDatas.get(i).get("BILL_TOTAL"), isSumBill) );
            }
            if("".equals(zyhjInfoDatas.get(i).get("THIRD_PAY")) || null==zyhjInfoDatas.get(i).get("THIRD_PAY")){
            	table.addCell(getPdfCell(""+0, isSumBill));
            }else{
            	table.addCell(getPdfCell(""+zyhjInfoDatas.get(i).get("THIRD_PAY"), isSumBill));
            }
            
            if("".equals(zyhjInfoDatas.get(i).get("CUT_PAY")) || null==zyhjInfoDatas.get(i).get("CUT_PAY")){
            	table.addCell(getPdfCell(""+0, isSumBill));
            }else{
            	table.addCell(getPdfCell(""+zyhjInfoDatas.get(i).get("CUT_PAY"), isSumBill));
            }
            if("".equals(zyhjInfoDatas.get(i).get("HOSP_CLAIMS")) || null==zyhjInfoDatas.get(i).get("HOSP_CLAIMS")){
            	table.addCell(getPdfCell(""+0, isSumBill));
            }else{
            	table.addCell(getPdfCell(""+zyhjInfoDatas.get(i).get("HOSP_CLAIMS"), isSumBill));
            }
            
           /* table.addCell(getPdfCell(""+zyhjInfoDatas.get(i).get("THIRD_PAY"), isSumBill));
            table.addCell(getPdfCell(""+zyhjInfoDatas.get(i).get("CUT_PAY"), isSumBill));
            table.addCell(getPdfCell(""+zyhjInfoDatas.get(i).get("HOSP_CLAIMS"), isSumBill));*/
       
           
        }
        table.setWidthPercentage(100);
        table.setHorizontalAlignment(Element.ALIGN_LEFT);
        return table;
    }
    
    /**
     * 住院合计信息
     * 
     * */
    private static PdfPTable getZyHjInfoHeader(int num, int [] width) throws DocumentException{
        PdfPTable table = new PdfPTable(num);
        String [] billTableHeader = {"申请金额合计","第三方支付金额合计","扣减金额合计","住院赔付合计"}; 
        table.setWidths(width); 
        for(int i = 0;i<billTableHeader.length;i++){
            PdfPCell cell = new PdfPCell(new Paragraph(billTableHeader[i], font8BOLD));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setBackgroundColor(new BaseColor(235,235,235));
            table.addCell(cell);
        }
        table.setWidthPercentage(100);
        table.setHorizontalAlignment(Element.ALIGN_LEFT);
        return table;
    }
    
    
    /**
     * 门诊信息结果集
     * */
    private static PdfPTable getMzInfoBody(List<Map<String, Object>> mzmxInfoDatas , int num, int [] width) throws DocumentException{
        PdfPTable table = new PdfPTable(num);
        table.setWidths(width); 
        boolean isSumBill = false;
        for(int i = 0; i<mzmxInfoDatas.size();i++){
            //需要数据转化
           // table.addCell(getPdfCell(""+billInfoDto.getClinDate())); 
            if(i==mzmxInfoDatas.size()-1){
                isSumBill = true;
            }
           
            
            if(mzmxInfoDatas.get(i).get("BILL_CODE")!=null){
            	 table.addCell(getPdfCell(""+mzmxInfoDatas.get(i).get("BILL_CODE"), isSumBill));
            }else{
            	 table.addCell(getPdfCell(""+"", isSumBill));
            }
            if(mzmxInfoDatas.get(i).get("HOSP_NM")!=null){
            	  table.addCell(getPdfCell(""+mzmxInfoDatas.get(i).get("HOSP_NM"), isSumBill));
           }else{
           	 table.addCell(getPdfCell(""+"", isSumBill));
           }
            if(mzmxInfoDatas.get(i).get("IN_HOSP_DATE")!=null){
            	  table.addCell(getPdfCell(""+mzmxInfoDatas.get(i).get("IN_HOSP_DATE"), isSumBill));
            }else{
         	 table.addCell(getPdfCell(""+"", isSumBill));
            }
           
          
          
            table.addCell(getPdfCell(""+mzmxInfoDatas.get(i).get("INV_AMO"), isSumBill));
            table.addCell(getPdfCell(""+mzmxInfoDatas.get(i).get("CUT_PAY"), isSumBill));
            table.addCell(getPdfCell(""+mzmxInfoDatas.get(i).get("THIRD_PAY"), isSumBill));
            table.addCell(getPdfCell(""+mzmxInfoDatas.get(i).get("COMP_PAY"), isSumBill));
            
            if(null==mzmxInfoDatas.get(i).get("CUT_PAY").toString() || "0".equals(mzmxInfoDatas.get(i).get("CUT_PAY").toString())){
            	 table.addCell(getPdfCell(""+"", isSumBill));
            }else{
            	 table.addCell(getPdfCell(""+mzmxInfoDatas.get(i).get("ITEM_NM"), isSumBill));
            }
            
            if(null!=mzmxInfoDatas.get(i).get("REFU_FLAG") && "1".equals(mzmxInfoDatas.get(i).get("REFU_FLAG").toString())){
           	 table.addCell(getPdfCell(""+"拒付", isSumBill));
           }else{
           	 table.addCell(getPdfCell(""+"", isSumBill));
           }
            
           
           
            //table.addCell(getPdfCell(""+mzmxInfoDatas.get(i).get("BZ"), isSumBill));
           
        }
        table.setWidthPercentage(100);
        table.setHorizontalAlignment(Element.ALIGN_LEFT);
        return table;
    }
    
    
    /**
     * 合计信息结果集
     * */
    private static PdfPTable getHjInfoBody(List<Map<String, Object>> hjInfoDatas , int num, int [] width) throws DocumentException{
        PdfPTable table = new PdfPTable(num);
        table.setWidths(width); 
        boolean isSumBill = false;
        for(int i = 0; i<hjInfoDatas.size();i++){
            //需要数据转化
           // table.addCell(getPdfCell(""+billInfoDto.getClinDate())); 
            if(i==hjInfoDatas.size()-1){
                isSumBill = true;
            }
           
            table.addCell(getPdfCell(""+hjInfoDatas.get(i).get("INV_AMO"), isSumBill));
            table.addCell(getPdfCell(""+hjInfoDatas.get(i).get("CUT_PAY"), isSumBill));
            table.addCell(getPdfCell(""+hjInfoDatas.get(i).get("COMP_PAY"), isSumBill));
            table.addCell(getPdfCell(""+hjInfoDatas.get(i).get("DZPJCBE"), isSumBill));
            table.addCell(getPdfCell(""+hjInfoDatas.get(i).get("YCBE"), isSumBill));
            table.addCell(getPdfCell(""+hjInfoDatas.get(i).get("MZCBE"), isSumBill));
           
        }
        table.setWidthPercentage(100);
        table.setHorizontalAlignment(Element.ALIGN_LEFT);
        return table;
    }
    
    
    /**
     * 理赔概要信息结果集
     * */
    private static PdfPTable getLpgyInfoBody(List<Map<String, Object>> lpgyInfoDatas , int num, int [] width) throws DocumentException{
        PdfPTable table = new PdfPTable(num);
        table.setWidths(width); 
        boolean isSumBill = false;
        for(int i = 0; i<lpgyInfoDatas.size();i++){
            //需要数据转化
           // table.addCell(getPdfCell(""+billInfoDto.getClinDate())); 
            if(i==lpgyInfoDatas.size()-1){
                isSumBill = true;
            }
           
            if(lpgyInfoDatas.get(i).get("PRODUCT_TYPE")!=null){
            	  // 员工
                if("1".equals(lpgyInfoDatas.get(i).get("INSU_TYPE")) && "1".equals(lpgyInfoDatas.get(i).get("PRODUCT_TYPE"))  ){
                	table.addCell(getPdfCell(""+"意外身故", isSumBill));
                }else if("1".equals(lpgyInfoDatas.get(i).get("INSU_TYPE")) && "2".equals(lpgyInfoDatas.get(i).get("PRODUCT_TYPE")) ){
                	table.addCell(getPdfCell(""+"意外伤残", isSumBill));
                }else if("1".equals(lpgyInfoDatas.get(i).get("INSU_TYPE")) && "3".equals(lpgyInfoDatas.get(i).get("PRODUCT_TYPE"))){
                	table.addCell(getPdfCell(""+"意外医疗", isSumBill));
                }
                // 子女
                if("2".equals(lpgyInfoDatas.get(i).get("INSU_TYPE"))){
                	table.addCell(getPdfCell(""+"疾病", isSumBill));
                }
                // 供养
                if("3".equals(lpgyInfoDatas.get(i).get("INSU_TYPE"))){
                	table.addCell(getPdfCell(""+"疾病", isSumBill));
                }
            }else{
            	table.addCell(getPdfCell(""+"", isSumBill));
            }
            table.addCell(getPdfCell(""+lpgyInfoDatas.get(i).get("MZZJE"), isSumBill));
            table.addCell(getPdfCell(""+lpgyInfoDatas.get(i).get("MZKJJE"), isSumBill));
            table.addCell(getPdfCell(""+lpgyInfoDatas.get(i).get("MZDSF"), isSumBill));
            table.addCell(getPdfCell(""+lpgyInfoDatas.get(i).get("MZPF"), isSumBill));
            table.addCell(getPdfCell(""+lpgyInfoDatas.get(i).get("ZYZJE"), isSumBill));
           
            table.addCell(getPdfCell(""+lpgyInfoDatas.get(i).get("ZYDSF"), isSumBill));
            table.addCell(getPdfCell(""+lpgyInfoDatas.get(i).get("ZYKJJE"), isSumBill));
            table.addCell(getPdfCell(""+lpgyInfoDatas.get(i).get("HOSP_CLAIMS"), isSumBill));
            
            BigDecimal mzzje=new BigDecimal(lpgyInfoDatas.get(i).get("MZZJE").toString());
            BigDecimal zyzje=new BigDecimal(lpgyInfoDatas.get(i).get("ZYZJE").toString());
            BigDecimal hjzje = mzzje.add(zyzje);  
            
            BigDecimal mzpf=new BigDecimal(lpgyInfoDatas.get(i).get("MZPF").toString());
            BigDecimal zypf=new BigDecimal(lpgyInfoDatas.get(i).get("HOSP_CLAIMS").toString());
            BigDecimal hjpf = mzpf.add(zypf);  
            
            // 意外身故
            if("1".equals(lpgyInfoDatas.get(i).get("INSU_TYPE")) && "1".equals(lpgyInfoDatas.get(i).get("PRODUCT_TYPE"))  ){
            	  table.addCell(getPdfCell(""+lpgyInfoDatas.get(i).get("BILL_TOTAL"), isSumBill));
                  table.addCell(getPdfCell(""+lpgyInfoDatas.get(i).get("COMP_PAY"), isSumBill));
            }else if("1".equals(lpgyInfoDatas.get(i).get("INSU_TYPE")) && "2".equals(lpgyInfoDatas.get(i).get("PRODUCT_TYPE")) ){
            	// 意外伤残
            	  table.addCell(getPdfCell(""+lpgyInfoDatas.get(i).get("BILL_TOTAL"), isSumBill));
                  table.addCell(getPdfCell(""+lpgyInfoDatas.get(i).get("COMP_PAY"), isSumBill));
            }else {
            	  table.addCell(getPdfCell(""+hjzje, isSumBill));
                  table.addCell(getPdfCell(""+hjpf, isSumBill));
            }
            
            
          
           
        }
        table.setWidthPercentage(100);
        table.setHorizontalAlignment(Element.ALIGN_LEFT);
        return table;
    }
    
    
    /**
     * 本次理赔概要
     * */
    private static void getLipeiInfoInfoTables(List<Map<String, Object>> lpgyInfoDatas, Document document) throws DocumentException, IOException{
    	  //放头部cell
        int num = 11;
        int [] width = new int[]{15,20,20,20,15,23,18,23,18,23,18};
        document.add(getLipeiInfoHeader(num, width));
        //放发票详情
       document.add(getLpgyInfoBody(lpgyInfoDatas, num, width));
        
    }
    
    /**
     * 住院理赔明细
     * */
    private static void getZyLipeiInfoInfoTables(List<Map<String, Object>> zymxInfoDatas, Document document) throws DocumentException, IOException{
    	  //放头部cell
        int num = 10;
        int [] width = new int[]{15,20,15,15,15,23,15,15,26,15};
        document.add(getZyLipeiInfoHeader(num, width));
        //放发票详情
          document.add(getZymxInfoBody(zymxInfoDatas, num, width));
        
    }
    
    /**
     * 门诊理赔明细
     * */
    private static void getMzLipeiInfoInfoTables(List<Map<String, Object>> mzmxInfoDatas, Document document) throws DocumentException, IOException{
    	  //放头部cell
        int num = 9;
        int [] width = new int[]{15,20,15,15,15,19,26,26,26};
        document.add(getMzLipeiInfoHeader(num, width));
        //放发票详情
       document.add(getMzInfoBody(mzmxInfoDatas, num, width));
    }
    
    /**
     * 合计信息
     * */
    private static void getHjInfoInfoTables(List<Map<String, Object>> hjInfoDatas, Document document) throws DocumentException, IOException{
    	  //放头部cell
        int num = 6;
        int [] width = new int[]{15,20,15,20,20,20};
        document.add(getHjInfoHeader(num, width));
        //放发票详情
        document.add(getHjInfoBody(hjInfoDatas, num, width));
        
    }
    
    /**
     * 投保信息
     * */
    private static PdfPTable getIllegalInfoHeader(int num, int [] width) throws DocumentException{
        PdfPTable table = new PdfPTable(num);
        String [] billTableHeader = {"被保人姓名","身份证号","申请员工姓名","员工编码","投保单位","投保类型","保险期间"}; 
        table.setWidths(width); 
        for(int i = 0;i<billTableHeader.length;i++){
            PdfPCell cell = new PdfPCell(new Paragraph(billTableHeader[i], font8BOLD));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setBackgroundColor(new BaseColor(235,235,235));
            table.addCell(cell);
        }
        table.setWidthPercentage(100);
        table.setHorizontalAlignment(Element.ALIGN_LEFT);
        return table;
    }
    
    private static PdfPTable getLipeiInfoHeader(int num, int [] width) throws DocumentException{
        PdfPTable table = new PdfPTable(num);
        String [] billTableHeader = {"费用类型","门诊申请总额","门诊扣减总额","门诊第三方支付","门诊赔付","住院申请总额","住院第三方支付","住院扣减总额","住院赔付","本次申请总金额","赔付总额"}; 
        table.setWidths(width); 
        for(int i = 0;i<billTableHeader.length;i++){
            PdfPCell cell = new PdfPCell(new Paragraph(billTableHeader[i], font8BOLD));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setBackgroundColor(new BaseColor(235,235,235));
            table.addCell(cell);
        }
        table.setWidthPercentage(100);
        table.setHorizontalAlignment(Element.ALIGN_LEFT);
        return table;
    }
    
    
    private static PdfPTable getBillInfoHeader(int num, int [] width) throws DocumentException{
        PdfPTable table = new PdfPTable(num);
        String [] billTableHeader = {"就诊时间","发票编码","就诊医疗机构","就诊类型","就诊疾病","票面金额","第三方支付金额","申请金额","理赔核减","赔付金额"}; 
        table.setWidths(width); 
        for(int i = 0;i<billTableHeader.length;i++){
            PdfPCell cell = new PdfPCell(new Paragraph(billTableHeader[i], font8BOLD));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setBackgroundColor(new BaseColor(235,235,235));
            table.addCell(cell);
        }
        table.setWidthPercentage(100);
        table.setHorizontalAlignment(Element.ALIGN_LEFT);
        return table;
    }
    
    /**
     * 住院理赔明细
     * 
     * */
    private static PdfPTable getZyLipeiInfoHeader(int num, int [] width) throws DocumentException{
        PdfPTable table = new PdfPTable(num);
        String [] billTableHeader = {"发票号","就诊医疗机构","入院时间","出院时间","申请金额","第三方支付金额","扣减金额","住院赔付","扣减明细-扣减原因-扣减类型-扣减金额","备注"}; 
        table.setWidths(width); 
        for(int i = 0;i<billTableHeader.length;i++){
            PdfPCell cell = new PdfPCell(new Paragraph(billTableHeader[i], font8BOLD));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setBackgroundColor(new BaseColor(235,235,235));
            table.addCell(cell);
        }
        table.setWidthPercentage(100);
        table.setHorizontalAlignment(Element.ALIGN_LEFT);
        return table;
    }
    
    /**
     * 门诊理赔明细
     * 
     * */
    private static PdfPTable getMzLipeiInfoHeader(int num, int [] width) throws DocumentException{
        PdfPTable table = new PdfPTable(num);
        String [] billTableHeader = {"发票号","就诊医疗机构","就诊时间","发票金额","扣减金额","第三方支付金额","赔付金额","扣减明细-扣减原因-扣减类型-扣减金额","备注"}; 
        table.setWidths(width); 
        for(int i = 0;i<billTableHeader.length;i++){
            PdfPCell cell = new PdfPCell(new Paragraph(billTableHeader[i], font8BOLD));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setBackgroundColor(new BaseColor(235,235,235));
            table.addCell(cell);
        }
        table.setWidthPercentage(100);
        table.setHorizontalAlignment(Element.ALIGN_LEFT);
        return table;
    }
    
    /**
     * 合计信息
     * 
     * */
    private static PdfPTable getHjInfoHeader(int num, int [] width) throws DocumentException{
        PdfPTable table = new PdfPTable(num);
        String [] billTableHeader = {"门诊合计","扣减合计","门诊赔付合计","单张票据超保额","月超保额","年度超保额"}; 
        table.setWidths(width); 
        for(int i = 0;i<billTableHeader.length;i++){
            PdfPCell cell = new PdfPCell(new Paragraph(billTableHeader[i], font8BOLD));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setBackgroundColor(new BaseColor(235,235,235));
            table.addCell(cell);
        }
        table.setWidthPercentage(100);
        table.setHorizontalAlignment(Element.ALIGN_LEFT);
        return table;
    }
    
    
    
    /*private static PdfPTable getBillInfoBody(List<PatrolCourtDTO> billInfoDtos, int num, int [] width) throws DocumentException{
        PdfPTable table = new PdfPTable(num);
        table.setWidths(width); 
        boolean isSumBill = false;
        for(int i = 0; i<billInfoDtos.size();i++){
            //需要数据转化
           // table.addCell(getPdfCell(""+billInfoDto.getClinDate())); 
            if(i==billInfoDtos.size()-1){
                isSumBill = true;
            }
            PatrolCourtDTO billInfoDto = billInfoDtos.get(i);
            table.addCell(getPdfCell(""+billInfoDto.getClinDate(), isSumBill)); 
            table.addCell(getPdfCell(""+billInfoDto.getBillCode(), isSumBill)); 
            table.addCell(getPdfCell(""+billInfoDto.getHosptailName(), isSumBill));  
            table.addCell(getPdfCell(""+billInfoDto.getClinType(), isSumBill)); 
            table.addCell(getPdfCell(""+billInfoDto.getDiagnosis(), isSumBill));
            table.addCell(getPdfCell(""+billInfoDto.getTotalCost(), isSumBill)); 
            table.addCell(getPdfCell(""+billInfoDto.getThirdPay(), isSumBill)); 
            table.addCell(getPdfCell(""+billInfoDto.getTotalCost(), isSumBill)); 
            table.addCell(getPdfCell(""+billInfoDto.getCutPay(), isSumBill)); 
            table.addCell(getPdfCell(""+billInfoDto.getCompPay(), isSumBill)); 
        }
        table.setWidthPercentage(100);
        table.setHorizontalAlignment(Element.ALIGN_LEFT);
        return table;
    }
    */
    /*private static PdfPTable getIllegalInfoBody(List<PatrolCourtDTO> illegalInfoDtos, int num, int [] width) throws DocumentException{
        PdfPTable table = new PdfPTable(num);
        table.setWidths(width); 
        for(PatrolCourtDTO illegalInfoDto : illegalInfoDtos){
            //需要数据转化
           // table.addCell(getPdfCell(""+billInfoDto.getClinDate())); 
            table.addCell(getPdfCell(""+billInfoDto.getClinDate())); 
            table.addCell(getPdfCell(""+billInfoDto.getBillCode())); 
            table.addCell(getPdfCell(""+billInfoDto.getHosptailName()));  
            table.addCell(getPdfCell(""+billInfoDto.getClinType())); 
            table.addCell(getPdfCell(""+billInfoDto.getDiagnosis()));
            table.addCell(getPdfCell(""+billInfoDto.getTotalCost())); 
            table.addCell(getPdfCell(""+billInfoDto.getThirdPay())); 
            table.addCell(getPdfCell(""+billInfoDto.getTotalCost())); 
            table.addCell(getPdfCell(""+billInfoDto.getCutPay())); 
            table.addCell(getPdfCell(""+billInfoDto.getCompPay())); 
        }
        
        for(int i=0;i<100;i++){
            table.addCell(getPdfCell("11111111")); 
            table.addCell(getPdfCell("11111111")); 
            table.addCell(getPdfCell("1111111111"));  
            table.addCell(getPdfCell("11111111")); 
            table.addCell(getPdfCell("11111111"));
            table.addCell(getPdfCell("11111111")); 
        }
        table.setWidthPercentage(100);
        table.setHorizontalAlignment(Element.ALIGN_LEFT);
        return table;
    }*/
    
    
    private static PdfPCell getPdfCell(String str, boolean isSumBill){
        PdfPCell cell = new PdfPCell(new Paragraph(str, font8NORMAL));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        if(isSumBill){
            cell.setBackgroundColor(new BaseColor(235,235,235));
        }
        return cell;
        
    }
    
    private static PdfPTable getSettlementInfoTable(Map<String, Object> settlementInfo, String [] settlementInfoArr, 
        int num, int width[] ) throws DocumentException, IOException{
        PdfPTable table = new PdfPTable(num);
        table.setWidths(width); 
        for(int i = 0;i<settlementInfoArr.length;i++){
            PdfPCell cell = new PdfPCell(new Paragraph(settlementInfoArr[i]+"："+settlementInfo.get(settlementInfoArr[i]),getFont10NORMAL()));
            cell.setBorder(0);
            table.addCell(cell);
        }
        table.setWidthPercentage(100);
        table.setHorizontalAlignment(Element.ALIGN_LEFT);
        return table;
    }

    
    private static Font getFont14BOLD() throws DocumentException, IOException{
        BaseFont baseFontChinese = BaseFont.createFont("STSong-Light", "UniGB-UCS2-H", BaseFont.NOT_EMBEDDED);
        return new Font(baseFontChinese, 14, Font.BOLD);
    }
    
    private static Font getFont10NORMAL() throws DocumentException, IOException{
        BaseFont baseFontChinese = BaseFont.createFont("STSong-Light", "UniGB-UCS2-H", BaseFont.NOT_EMBEDDED);
        return new Font(baseFontChinese, 10, Font.BOLD);
    }
    
    private static Font getFont5NORMAL() throws DocumentException, IOException{
        BaseFont baseFontChinese = BaseFont.createFont("STSong-Light", "UniGB-UCS2-H", BaseFont.NOT_EMBEDDED);
        return new Font(baseFontChinese, 5, Font.BOLD);
    }
    
    private static Font getFont8BOLD() throws DocumentException, IOException{
        BaseFont baseFontChinese = BaseFont.createFont("STSong-Light", "UniGB-UCS2-H", BaseFont.NOT_EMBEDDED);
        return new Font(baseFontChinese, 8, Font.BOLD);
    }
    
    private static Font getFont8NORMAL() throws DocumentException, IOException{
        BaseFont baseFontChinese = BaseFont.createFont("STSong-Light", "UniGB-UCS2-H", BaseFont.NOT_EMBEDDED);
        return new Font(baseFontChinese, 8, Font.NORMAL);
    }
    
    
    /**
     * 换行操作
     */
    private static void wrapHandle(Document document) throws DocumentException{
        Paragraph blankRow = new Paragraph(18f, " ", font14BOLD); 
        document.add(blankRow);
        
    }
    
    /**
     * 头部标题
     * @throws IOException 
     * @throws DocumentException 
     */
    private static void setHeaderTitle(Document document) throws DocumentException, IOException{
        Paragraph paragraph = new Paragraph();  
        paragraph.setAlignment(Element.ALIGN_CENTER);  
        Chunk chunk = new Chunk("员工福利项目理赔通知书");
        paragraph.setFont(getFont14BOLD());
        paragraph.add(chunk);  
        document.add(paragraph);  
    }
    
    private static Paragraph getParagraph(String str, int element, Font font, float spacing){
        Paragraph paragraph = new Paragraph();  
        paragraph.setAlignment(element);  
        Chunk chunk = new Chunk(str);
        paragraph.setFont(font);
        paragraph.add(chunk);  
        paragraph.setSpacingAfter(spacing);
        return paragraph;
    }
    
    private static void setLineSeparator(Document document) throws DocumentException{
        Paragraph paragraph = new Paragraph();  
        paragraph.add(new Chunk(new LineSeparator()));  
        document.add(paragraph);
    }
    
    
    //获取项目部署的webRoot文件夹路径
    public static String getClassPath() {
        //classes文件路径
        File classesDir = new File(CreateClaimPdfUtil.class.getClassLoader().getResource("").getPath());
        //项目WebRoot目录
        File webDir = classesDir.getParentFile().getParentFile();
        return webDir.getAbsolutePath().replace("%20", " ");
    }

}

