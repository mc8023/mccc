 package com.mc.mccc.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.mc.mccc.enter.SysMenu;
import com.mc.mccc.enter.SysUser;
import com.mc.mccc.service.impl.HomePageServiceImpl;
import com.mc.mccc.shiro.ApplicationConstants;

/**
 * 首页（主入口）
 * @author luomengzhu
 */
@Controller
@RequestMapping("/homePage")
public class HomePageController {
	@Resource
	private HomePageServiceImpl homePageServiceImpl;
	
	@RequestMapping("/home")
    public String login() {
        return "jsp/homePage";
    }
	
	@RequestMapping("/index")
    public String index(){
        return "jsp/index";
    }
    
    @RequestMapping("/page")
    public String page(){
        return "jsp/page";
    }
    
    @RequestMapping("/head")
    public String head(){
        return "comm/_header";
    }
    
    @RequestMapping("/footer")
    public String footer(){
        return "comm/_footer";
    }
    
	@RequestMapping("/leftMenu")
    public ModelAndView leftMenu(HttpSession session, ModelAndView mv){
        SysUser user = (SysUser) session.getAttribute(ApplicationConstants.LOGIN_USER);
        //通过userId 获取他所有权限内的菜单
        mv.setViewName("comm/_left");
        List<SysMenu> menuList = homePageServiceImpl.findSysMenuByUserId(user.getId());
        List<SysMenu> curFuncList = new ArrayList<SysMenu>();
        if(CollectionUtils.isNotEmpty(menuList)){
            //目前只做两层目录菜单，先简单写下，如果需要多级菜单在自行递归吧
            for(SysMenu sysMenu : menuList){
                //默认为第一级菜单
                if(sysMenu.getMenu_level()==1){
                    curFuncList.add(sysMenu);
                    sysMenu.setChildSysFuncs(getChildFuncList(menuList, sysMenu));
                }
            }
            mv.addObject("funcList", curFuncList);
        }
        return mv;
    }


	/**
     * 获取当前菜单的子菜单
     * @param funcList
     * @param sysFunc
     * @return
     */
    private List<SysMenu> getChildFuncList(List<SysMenu> menuList, SysMenu sysMenu){
        List<SysMenu> childMenuList = new ArrayList<SysMenu>();
        for(SysMenu _sysMenu : menuList){
            if(_sysMenu.getParent() == sysMenu.getId()){
            	childMenuList.add(_sysMenu);
            }
        }
        return childMenuList;
    }
}
