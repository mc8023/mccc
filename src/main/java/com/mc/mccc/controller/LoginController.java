package com.mc.mccc.controller;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Enumeration;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.mgt.RealmSecurityManager;
import org.apache.shiro.realm.AuthenticatingRealm;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import com.mc.mccc.enter.SysUser;
import com.mc.mccc.mapper.SysUserMapper;
import com.mc.mccc.shiro.ApplicationConstants;

import org.apache.shiro.web.servlet.SimpleCookie;

/**
 * 登录
 * 
 * @author cuihom
 *
 */
@Controller
public class LoginController {
	@Resource
	private SysUserMapper sysUserMapper;
	
	@Autowired
	private SimpleCookie sessionManager;
	
	@Autowired
	private RealmSecurityManager securityManager;
	
	
	@RequestMapping("/login")
	public String login(HttpServletRequest request, Model model, String username, String password) throws InterruptedException {
		initSession(request);
//		TimeUnit.MILLISECONDS.sleep(10000);
		try {
			if (StringUtils.isNotBlank(username)) {
				username = new String(username.getBytes("ISO-8859-1"), "UTF-8");
			}
			if (StringUtils.isNotBlank(password)) {
				password = new String(password.getBytes("ISO-8859-1"), "UTF-8");
			}
				
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		UsernamePasswordToken token = new UsernamePasswordToken(username, password);
		token.setRememberMe(false);
		
		Subject subject = SecurityUtils.getSubject();
		
	   try { 
		   
		   subject.login(token);  
	        return loginSuccess();
	    } catch (AuthenticationException e) {  
	    	model.addAttribute("username", username);
			request.setAttribute("yhm", username);
			return "jsp/login/login";
	    }  
	}
	
	
	 public void userpy(){  
		 
		 List<SysUser> list = sysUserMapper.getuserlist();
		 
		 for(int i=0;i<list.size();i++){
			 
			 String str1=list.get(i).getIdCard();
			 String str2=EncoderByMd5(list.get(i).getPym()) ;
			 String str3=list.get(i).getQuery_condition();
			 String str4=list.get(i).getPhone();
			 sysUserMapper.insertuser(str1,str2,str3,str4);
		 }
		 
		 
		 
	 }
	
	

	/**
	 * 利用MD5进行加密
	 * 
	 * @param str
	 *            待加密的字符串
	 * @return 加密后的字符串
	 * @throws NoSuchAlgorithmException
	 *             没有这种产生消息摘要的算法
	 * @throws UnsupportedEncodingException
	 */
	public String EncoderByMd5(String str) {
		MessageDigest md;
		StringBuffer sb = new StringBuffer();
		try {
			md = MessageDigest.getInstance("MD5");
			md.update(str.getBytes());
			byte[] data = md.digest();
			int index;
			for (byte b : data) {
				index = b;
				if (index < 0)
					index += 256;
				if (index < 16)
					sb.append("0");
				sb.append(Integer.toHexString(index));
			}
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		System.out.println(sb.toString());
		return sb.toString();
	}

	/**
	 * 登录成功方法，通过重定向跳转到主页面
	 *
	 * @return
	 */
	@RequestMapping({ "/", "/main" })
	@RequiresAuthentication
	public String loginSuccess() {
		Subject subject = SecurityUtils.getSubject();
		SysUser user = (SysUser) subject.getPrincipal();
		user = sysUserMapper.getSysUserByUsername(user.getUsername());
		Session session = subject.getSession();
		// 把用户角色放进session
		session.setAttribute(ApplicationConstants.LOGIN_USER, user);
		session.setAttribute("yhm", user.getUsername());
		// 转发请求
		return InternalResourceViewResolver.FORWARD_URL_PREFIX + "/homePage/index";

	}

	/**
	 * 用户登出方法
	 *
	 * @return 打开登出页面
	 * @throws InterruptedException 
	 */
	@RequestMapping("/logout")
	public String logout(HttpServletResponse response) throws InterruptedException {

		Subject subject = SecurityUtils.getSubject();
		 if (subject.isAuthenticated()) {
				subject.logout(); // session 会销毁
		}
		   
//		 Cookie cookie = new Cookie("*.session.id", "");
//		 response.addCookie(cookie);
		 return "redirect:/login";
		
//		 TimeUnit.MILLISECONDS.sleep(10000);
		
	}

	/**
	 * 用户登出方法
	 *
	 * @return 打开登出页面
	 * @throws InterruptedException 
	 */
	@RequestMapping("/logoutnew")
	public String logoutnew(ServletRequest request,ServletResponse response) throws InterruptedException {

		Subject subject = SecurityUtils.getSubject();
		 if (subject.isAuthenticated()) {
				subject.logout(); // session 会销毁
			}
		 return "jsp/login/login";
		
		
	}

	
	
	/**
	 * 没有权限访问的页面
	 * 
	 * @return
	 */
	@RequestMapping("/noPermission")
	public String noPermission() {
		return "jsp/login/noPermission";
	}

	/**
	 * 清空session
	 * 
	 * 
	 */
	private void initSession(HttpServletRequest request) {
		Enumeration em = request.getSession().getAttributeNames();
		while (em.hasMoreElements()) {
			request.getSession().removeAttribute(em.nextElement().toString());
		}
	}

}
