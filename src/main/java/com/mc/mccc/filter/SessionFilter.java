package com.mc.mccc.filter;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;

import com.mc.mccc.enter.SysUser;
import com.mc.mccc.shiro.ApplicationConstants;

public class SessionFilter implements Filter {
	private String excludedPages;
	private String[] excludedPageArray;

	public void destroy() {
		// 过滤器销毁，一般是释放资源
	}

	/**
	 * 某些url需要登陆才能访问（session验证过滤器）
	 */
	public void doFilter(ServletRequest arg0, ServletResponse arg1, FilterChain arg2)
			throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) arg0;
		HttpServletResponse response = (HttpServletResponse) arg1;
		Subject subject = SecurityUtils.getSubject();
		Session session = subject.getSession();
		// HttpSession session = request.getSession();
		// 判断session是否过期
		boolean isExcludedPage = false;
		for (String page : excludedPageArray) {// 判断是否在过滤url之外
			if (((HttpServletRequest) request).getServletPath().equals(page)) {
				isExcludedPage = true;
				break;
			}
		}
		if (isExcludedPage) {
			if ((SysUser) session.getAttribute(ApplicationConstants.LOGIN_USER) == null) {
				// String errors = "您还没有登录，或者session已过期。请先登陆!";
				// request.setAttribute("Message", errors);
				// 跳转至登录页面
				request.getRequestDispatcher("/login.jsp").forward(request, response);
			} else {
				arg2.doFilter(request, response);
			}
		}else {
			arg2.doFilter(request, response);
		}
	}

	public void init(FilterConfig fConfig) throws ServletException {
		// 初始化操作，读取web.xml中过滤器配置的初始化参数，满足你提的要求不用此方法
		excludedPages = fConfig.getInitParameter("excludedPages");
		if (StringUtils.isNotEmpty(excludedPages)) {
			excludedPageArray = excludedPages.split(",");
		}
		return;
	}
}
