package com.mc.mccc.comm;

/**
 * 系统常用类
 * @author tiansheng
 *
 */
public class Constant {
	
	/** 有效（可用）状态*/
	public static final int STATUS_EFFECTIVE=0;
	
	/** 无效（不可用）状态*/
	public static final int STATUS_INVALID=1;
	
}
