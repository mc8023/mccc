/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mc.mccc.comm;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.alibaba.fastjson.JSONObject;

/**
 *
 * @author tiansheng
 */
public class AjaxMessage {

	private final List<String> error = new ArrayList<String>();
	private final List<String> msg = new ArrayList<String>();
	private Object data;

	public boolean isSuccess() {
		return !hasError();
	}

	public boolean hasError() {
		return !error.isEmpty();
	}

	public AjaxMessage addError(String message) {
		error.add(message);
		return this;
	}

	public List<String> getErrors() {
		return Collections.unmodifiableList(error);
	}

	public String getErrors(int index) {
		return error.get(index);
	}

	public AjaxMessage clearErrors() {
		error.clear();
		return this;
	}

	public AjaxMessage clearError(int index) {
		error.remove(index);
		return this;
	}

	public boolean hasMessage() {
		return msg.size() > 0;
	}

	public AjaxMessage addMessage(String message) {
		msg.add(message);
		return this;
	}

	public List<String> getMessages() {
		return Collections.unmodifiableList(msg);
	}

	public String getMessages(int index) {
		return msg.get(index);
	}

	public AjaxMessage clearMessages() {
		msg.clear();
		return this;
	}

	public AjaxMessage clearMessage(int index) {
		msg.remove(index);
		return this;
	}

	public Object getData() {
		return data;
	}

	public AjaxMessage setData(Object data) {
		this.data = data;
		return this;
	}

	public String toJsonstring() {
		return JSONObject.toJSONString(this);
	}
}
