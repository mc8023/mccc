package com.mc.mccc.comm.bean;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

public class PageParamBean {
	
	private int pageNum;
	
	private int pageSize;
	
	public PageParamBean(HttpServletRequest request) {
		pageNum = StringUtils.isBlank(request.getParameter("pageNum"))?1:Integer.parseInt(request.getParameter("pageNum"));
		pageSize = 100;
	}

	public int getPageNum() {
		return pageNum;
	}

	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	
	
	

}
