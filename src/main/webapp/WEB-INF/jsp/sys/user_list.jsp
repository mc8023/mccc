<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="UTF-8" />
<title>菜单管理</title>
<%@include file="/WEB-INF/comm/_env.jsp"%>
</head>
<body>
<script type="text/javascript" charset="utf-8">
 
</script>
<div id="main-content">
		<div class="container">
			<div class="row">
				<div id="content" class="col-lg-12">
					<!-- PAGE HEADER-->
					<div class="row">
						<div class="col-sm-12">
							<div class="page-header">
								<ul class="breadcrumb">
								</ul>
								<form action="${ROOT}/sys/user.html" class="form-inline"
									role="form">
										<div class="form-group normal-form">
											<div class="col-sm-12">
												    <label for="exampleInputName2 lable70">用户名</label>
												    <input type="text" class="form-control width200" id="username_param" name="username_param" placeholder="username" value="${username_param}">
										  	</div>
										</div>
									<button type="submit" class="btn btn-primary-cx">查询</button>
								</form>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="box border green" style="padding: 0px 0px 1% 0px;">
								<div class="box-title">
									<h4>
										<i class="fa fa-table"></i>系统用户
									</h4>
									<div style="float: right;">
										<button class="btn btn-xs btn-info" onclick="editUser(0,this);"><i class="fa fa-plus"></i>添加</button>
									</div>
								</div>
								<div class="box-body">
									<table id="datatable" style="font-size: 13px;" cellpadding="0"
										cellspacing="0" border="0"
										class="datatable table table-striped table-bordered table-hover">
										<thead>
											<tr>
												<th>ID</th>
												<th>用户名</th>
												<th>状态</th>
												<th>Email</th>
												<th>联系方式</th>
												<th>操作</th>
											</tr>
										</thead>
										<tbody>
											<c:forEach items="${pageResult.pageList }" var="item">
												<tr${item.id}>
													<td>${item.id }</td>
													<td>${item.username }</td>
													<td class="center">
														<c:choose>
															<c:when test="${item.status==0}">
																有效
															</c:when>
															<c:when test="${item.status==1}">
																无效
															</c:when>
															<c:otherwise>
																未知
															</c:otherwise>
														</c:choose>
													</td>
													<td class="center">${item.email }</td>
													<td class="center">${item.phone }</td>
													<td class="center hidden-xs">
														<a class="btn btn-xs btn-info" href="javascript:editUser(${item.id}),this">
															<i class="fa fa-pencil-square-o"></i> 编辑</a>
															<a class="btn btn-xs btn-info" href="javascript:editpassword(${item.id}),this">
															<i class="fa fa-pencil-square-o"></i> 重置密码</a>
														<a class="btn btn-xs btn-danger" href="javascript:delConfirm(${item.id});">
															<i class="fa fa-trash-o"></i> 删除</a>
														<a class="btn btn-xs btn-success" href="javascript:editUserRole(${item.id}),void(0)">
															<i class="fa fa-check-square-o"></i> 分配角色</a>
													</td>
												</tr>
											</c:forEach>
										</tbody>
									</table>
									<div id="paging1" class="page" style="margin-top: 1%"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<!-- Edit Modal -->
	<div class="modal fade" id="editUserModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="myModalLabel">用户编辑</h4>
				</div>
				<form action="${ROOT}/sys/user/post" id="editForm"
					class="form-horizontal" role="form" >
					<div id="tips" align="center"></div>
					<div class="modal-body">
						<div class="form-group">
							<label class="col-sm-3 control-label">ID：</label>
							<div class="col-sm-9">
								<input class="form-control input-sm" type="text" name="id" id="id" readonly>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">用户名：</label>
							<div class="col-sm-9">
								<input class="form-control input-sm" type="text" name="username" id="username"
									placeholder="请输入用户名">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">密码：</label>
							<div class="col-sm-9">
								<input class="form-control input-sm" type="password"
									name="password" id="password" placeholder="请输入用户名">
							</div>
						</div>
						</p>
						<div class="form-group">
							<label class="col-sm-3 control-label">身份证号：</label>
							<div class="col-sm-9">
								<input class="form-control input-sm" type="text" name="idCard" id="idCard"
									placeholder="请输入身份证号">
							</div>
						</div>
					<!-- 	<div class="form-group">
							<label class="col-sm-3 control-label">所属机构：</label>
							<div class="col-sm-9">
								<select class="form-control" name="insuOrgId" id="insuOrgIdSelect">
									<option value="-1">---请选择机构---</option>
									<c:forEach items="${compList }" var="item">		
										<option value="${item.insuOrgId }">${item.insuOrgNm}</option>
									</c:forEach>
								</select>
							</div>
						</div>  -->
						
						
						<div class="form-group">
						<label class="col-sm-3 control-label">所属机构：</label>
						<div class="col-sm-9">
						<input  class="form-control input-sm" type="text" name="makeupCo" id="makeupCo" class="makeinp" onfocus="setfocus(this)" oninput="setinput(this);" placeholder="请选择或输入"/>  
							    <select  class="form-control"  name="insuOrgId" id="insuOrgIdSelect" onchange="changeF(this)" size="10" style="display:none;">  
							        <c:forEach items="${compList }" var="item">		
										<option value="${item.insuOrgId }">${item.insuOrgNm}</option>
									</c:forEach> 
							    </select>  
						</div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-3 control-label">用户状态：</label>
							<div class="col-sm-9">
								<label class="radio-inline"> <input type="radio" id="rad11"
									class="uniform" name="status" value="0" checked="checked"> 有效
								</label> <label class="radio-inline"> <input type="radio" id="rad12"
									class="uniform" name="status" value="1"> 无效
								</label>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Email：</label>
							<div class="col-sm-9">
								<input class="form-control input-sm" type="text" name="email"
									placeholder="请输入Email">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Phone：</label>
							<div class="col-sm-9">
								<input class="form-control input-sm" type="text" name="phone"
									placeholder="请输入Phone">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">用户权限：</label>
							<div class="col-sm-9">
								<select class="form-control" name="person_level" id="person_level" onclick="xzjgid()">
									<option value="-1">---请选择权限---</option>
									<option value="1">系统管理员</option>
									<option value="2">板块</option>
									<option value="3">单位</option>
									<option value="4">员工</option>
									<option value="5">机构管理员</option>
								</select>
							</div>
							<label class="col-sm-7 control-label"><span style="color:red">（请先选择机构再选择权限）</span></label>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">权限名称代码</label>
							<div class="col-sm-9">
								<input class="form-control input-sm" type="text" name="query_condition" id="query_condition"
									placeholder="请输入单位丶版块或员工的对应编码。系统管理员请输入0">
							</div>
						</div>
						
						<div class="modal-body" id="dxjgdiv">
						<hr color="red" width="100%" >
					<div class="form-group" style="margin-left:-60px;matgin-top:30px">
					   <label class="col-sm-3 control-label">机构名称</label>
					   <div class="col-sm-9">
						<input class="form-control input-sm" type="text"  onkeyup="jgmhfind(this)">
					 </div>
					</div>
						
						
					
					<div style="height: 300px; width: 100%; overflow: auto;">
							<table class="table table-hover" id='user_dxjg_tab'>
							<thead>
							  <tr align="left">
								<th align="left">选择</th>
								
							  </tr>
							</thead>
							<tbody>
							  <c:forEach items="${compList}" var="item">
								  <tr>
									<td><input type="checkbox" name="dxjg" class='uniform' value='${item.insuOrgId }' ></td>
									<td>${item.insuOrgId }</td>
									<td>${item.insuOrgNm }</td>
								  </tr>
							  </c:forEach>
							</tbody>
						  </table>	
				  </div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
						<button class="btn btn-primary" onclick="keep_close()">提交并关闭</button>
					</div>
				</form>
			</div>
		</div>
	</div>


	



	<!-- delete Modal -->
	<div class="modal fade" id="deleteUserModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel">
		<input type="hidden" id="delId" value="" />
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="myModalLabel">用户编辑</h4>
				</div>
				<div class="modal-body">
				</div>
				<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<!-- <button type="submit" class="btn btn-primary">保存</button> -->
						<button class="btn btn-danger" onclick="del()">确认删除</button>
					</div>
			</div>
		</div>
	</div>
	
	<!-- delete Modal -->
	<div class="modal fade" id="eidtpswModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel">
		<input type="hidden" id="psId" value="" />
		<input type="hidden" id="psNm" value="" />
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="myModalLabel">重置密码</h4>
				</div>
				<div class="modal-body">
				</div>
				<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
						<button class="btn btn-danger" onclick="restpsw()">确认</button>
					</div>
			</div>
		</div>
	</div>
	

	<!-- Edit User_Role -->
	<div class="modal fade" id="editUserRoleModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
		<input type="hidden" id="userId" name="userId" value="">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="myModalLabel">选择角色</h4>
				</div>
				<div class="modal-body">
					<table class="table table-hover" id='user_role_tab'>
					<thead>
					  <tr>
						<th><input type="checkbox" name="selectRoleAll" id="selectRoleAll" onchange="selectAllRoles();">全选</th>
						<th>角色名称</th>
						<th>状态</th>
						<th>描述</th>
					  </tr>
					</thead>
					<tbody>
					  <c:forEach items="${roleList}" var="item">
						  <tr>
							<td><input type="checkbox" name="ids" class='uniform' value='${item.id }' ></td>
							<td>${item.name }</td>
							<td>${item.status==0?'有效':'无效' }</td>
							<td>${item.description }</td>
						  </tr>
					  </c:forEach>
					</tbody>
				  </table>	
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
					<button class="btn btn-primary" type="button" onclick="submit_close_role(this)">提交并关闭</button>
				</div>
			</div>
		</div>
	</div>
	
	
		<!-- Edit Role Func -->
	<div class="modal fade" id="editjgModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
		<input type="hidden" id="userjgId" name="userjgId" value="">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="myModalLabel">选择机构</h4>
					
					
				</div>
				<div class="modal-body">
						<table class="table table-hover" id='user_jg_tab'>
						<thead>
						  <tr>
							<th><input type="checkbox" name="selectjgAll" id="selectjgAll" onchange="selectjgAll">全选</th>
							<th>机构ID</th>
							<th>机构名称</th>
						  </tr>
						</thead>
						<tbody>
						<c:forEach items="${compjgList}" var="item"  varStatus="status">
						  <tr>
							<td><input type="checkbox"  class='uniform' value='${item.insuOrgId }' id="demo" onclick="selectAll('${item.name}');"></td>
							<td>${item.insuOrgId }</td>
							<td>${item.insuOrgNm}</td>
						  </tr>
						 </c:forEach>
						</tbody>
					  </table>
				</div>
				<div class="modal-footer">
					<!-- <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
					<button class="btn btn-primary" onclick="submit_close_func(this)">提交并关闭</button>  -->
				</div>
			</div>
		</div>
	</div>
	
</body>
<script type="text/javascript" charset="utf-8">

$(function () {
	$("#paging1").pagination({
		items : parseInt('${pageResult.pages}'),
		currentPage : parseInt('${pageResult.pageNum}'),
		cssStyle : "light-theme",
		prevText : "上一页",
		nextText : "下一页",
		hrefTextPrefix : "${ROOT}/sys/user.html?pageSize=${pageResult.pageSize}&total=${pageResult.total}&pageNum="
	});
	
	
    var ajax = null, $fm = $('#editUserModal form:first'), $md = $('#editUserModal');
    window.editUser = function (id,dom) {
    	var mytable = document.getElementById('user_dxjg_tab');
		for (var i = 1; i < mytable.rows.length; i++) {
			mytable.rows[i].style.display = "block";
		}
        var fm = $fm[0];
        if (ajax)
            ajax.abort();
        fm.reset();
        ajax = $.getJSON("${ROOT}/sys/user/get/" + id, function (json) {
        	
            if (id != 0 && !json.data) {
                return alert("找不到用户");
            }
            if (id == 0 && !json.data) {
                $md.find(".modal-header h4").html("新增用户");
                json = $.extend(true, {}, json.data, {data: {id: 0}});
                $("#rad11").attr("checked",true); 
                $("#dxjgdiv").hide();
            } else {
                $md.find(".modal-header h4").html("编辑用户 - " + (json.data.username || ""));
                if(json.data.status==0){
                	rad11.checked = "checked";
                }else{
                	rad12.checked = "checked";
                }
                
                if(json.data.insuOrgNm!=null&&json.data.insuOrgNm.length>0){
                	$("#makeupCo").val(json.data.insuOrgNm);
                }
                
             
               if(json.data.person_level=="5"){
                 	$("#dxjgdiv").show();
               }else{
                 	$("#dxjgdiv").hide();
               }
                
            }
            
           
            $("#user_dxjg_tab tbody :checkbox").each(function(){
    			$(this).removeAttr("checked");
        	});
    		
    	if(json.data.company!=null&&json.data.company.length>0){
			var content = json.data.company;
    		$("#user_dxjg_tab tbody :checkbox").each(function(){
    			var the = $(this);
    			for(var i = 0;i<content.length;i++){
    				if(the.val()==content[i].insuorg){
    					$(this).prop("checked","checked");
    				}
    			}
        	});
        }
            
            
            var $fm = $(fm);
            $fm.unSerializeObjectFromJson(json.data);
            $("#editUserModal .progress").hide();
            $fm.removeClass("hide");
        });
        $md.modal();
    };

    $md.on('hidden', function (e) {
        $("#editUserModal .progress").show();
        $fm.addClass("hide");
    });
    $.request($fm, function (json) {
        if (json.success) {
           
            window.location.reload();
        }
        if (ajax.__close) {
            $md.modal("hide");
        }
    });
    window.submit_close = function (dom) {
    	
    	var flag = true;
    	var username = $("#username").val();
    	if(username==""){
    		$("#username").focus();
    		flag = false;
    		//return alert("用户名为必填项!");
    		
    		 //return false;
    	}
    	var password = $("#password").val();
    	if(password==""){
    		$("#password").focus();
    		flag = false;
    		 //return alert("密码为必填项!");
    		// flag = false;
    		
    		 //return false;
    	}
    	if(password.length<=15||password.length>=18){
    		$("#password").focus();
    		flag = false;
    		 //return alert("密码为必填项!");
    		// flag = false;
    		
    		 //return false;
    	}
    	var idcard = $("#idCard").val();
    	if(idcard==""){
    		$("#idCard").focus();
    		flag = false;
    	}
    	
    	var insuOrgIdSelect = $("#insuOrgIdSelect").val();
    	if(insuOrgIdSelect=="-1"){
    		 $("#insuOrgIdSelect").focus();
    		 flag = false;
    		 //return alert("请选择所属机构!");
    		 //flag = false;
    		
    		 //return false;
    	}
    	var person_level = $("#person_level").val();
    	if(person_level=="-1"){
    		 $("#person_level").focus();
    		 flag = false;
    		//return alert("请选择用户权限!");
    		 //flag = false;
    		 
    		 //return false;
    	}
    	var query_condition = $("#query_condition").val();
    	if(query_condition==""){
    		$("#query_condition").focus();
    		flag = false;
    		// return alert("权限名称代码为必填项!");
    		 //flag = false;
    		  
    		// return false;
    	}
    	
    	var id_array = new Array();
    	$("#user_dxjg_tab tbody :checkbox").each(function(i){
    		if($(this).is(":checked")){
    			id_array.push($(this).val())	
    		}  
    	});
    	if(id_array.length<=0){
    		flag = false;
    		return false;
    	}
    	 
           if(flag){
        	   var the = $(dom);
               the.closest(".modal").find("form:first").submit();
               ajax.__close = true;
               window.close();
           }
		
    };
    
    function keepuserjgxx(){
    	
    	var id_array = new Array();
    	$("#user_dxjg_tab tbody :checkbox").each(function(i){
    		if($(this).is(":checked")){
    			id_array.push($(this).val())	
    		}  
    	});
    	if(id_array.length<=0){
    		alert("请选择机构！");
    		return;
    	}
    	
    	$.ajax({
    		url:"${ROOT}/sys/user_role/keepjg/",
    		type:"POST",
    		dataType:"json",
    		data:"ids="+id_array+"&userId="+$("#userId").val(),
    		success:function(json){
    			if(json.success){
    				alert("保存成功！");
        			$("#editUserRoleModal").modal("hide");
    			}else{
    				alert("保存失败！");
    			}
    		}
    	});
    	
    }

    
     editpassword=function(id){
    	 $("#eidtpswModal .modal-body").html("确定重置密码吗?");
     	 $('#eidtpswModal').modal("show");
     	 $("#psId").val(id);
 }
     restpsw=function(){
    	 $.ajax({
        		url: "${ROOT}/sys/user/restpsw/",      		
        		type:"POST",
        		dataType:"json",
        		data:"id="+$("#psId").val()+"&username="+$("#psNm").val(),
        		success:function(json){
        			if(json.success){
     				alert("保存成功！");
         			$("#eidtpswModal").modal("hide");
     			}else{
     				alert("保存失败！");
     			}
        		}
        	});

    	 
     }
    
     
    delConfirm=function(id){
    	$("#deleteUserModal .modal-body").html("确定删除id为"+id+"的记录吗?");
    	$('#deleteUserModal').modal("show");
    	$("#delId").val(id);
    }
    del=function(){
    	var delId  = $("#delId").val();
    	 $.ajax({
			type : "POST",
			url : "${ROOT}/sys/user/delete/"+delId,
			dataType : "json",
			success : function(json) {
				if(json.success){
					alert("删除成功！");
					window.location.reload();
					$("#tr"+delId).remove();
					$("#deleteUserModal").modal("hide");
				}else {
					alert(data.error);
				}
			}
		}); 
    }
    window.editUserRole = function (id) {
    	$("#userId").val(id);
    	$.getJSON("${ROOT}/sys/user_role/getByUserId/" + id, function (json) {
    		$("#user_role_tab tbody :checkbox").each(function(){
    			$(this).removeAttr("checked");
        	});
    		 
    		
			var content = json.data;
    		$("#user_role_tab tbody :checkbox").each(function(){
    			var the = $(this);
    			for(var i = 0;i<content.length;i++){
    				if(the.val()==content[i].roleId){
    					$(this).prop("checked","checked");
    				}
    			}
        	});
    	    $("#editUserRoleModal").modal();
    	});	
    }
    
     selectAllRoles=function(){
    	
    	    	// 全选
    	    	var allDom = document.getElementsByName("selectRoleAll")[0];  //找到全选框
    	    	var numDoms = document.getElementsByName("ids"); //找到选项的数组

    	    	allDom.onclick = function(){   //定义一个全选框的点击函数
    	    	if(this.checked){                 //判断全选框是否选中,如果返回值为true代表选中
    	    			for(var i=0;i<numDoms.length;i++){   //使用for循环选中列表框
    	    				numDoms[i].checked = true;
    	    				//$('#daochu').removeAttr("disabled");
    	    			}
    	    	}else{                                             //如果返回值为false代表未选中
    	    			for(var i=0;i<numDoms.length;i++){  //使用for循环取消列表框的选中状态
    	    				numDoms[i].checked = false;
    	    				//$('#daochu').attr('disabled',"true");
    	    			}
    	    		}
    	    	}
    		
	} 
     

	    	// 全选
	    	var allDom = document.getElementsByName("selectRoleAll")[0];  //找到全选框
	    	var numDoms = document.getElementsByName("ids"); //找到选项的数组

	    	allDom.onclick = function(){   //定义一个全选框的点击函数
	    	if(this.checked){                 //判断全选框是否选中,如果返回值为true代表选中
	    			for(var i=0;i<numDoms.length;i++){   //使用for循环选中列表框
	    				numDoms[i].checked = true;
	    				//$('#daochu').removeAttr("disabled");
	    			}
	    	}else{                                             //如果返回值为false代表未选中
	    			for(var i=0;i<numDoms.length;i++){  //使用for循环取消列表框的选中状态
	    				numDoms[i].checked = false;
	    				//$('#daochu').attr('disabled',"true");
	    			}
	    		}
	    	}
		
	
	
    
    window.submit_close_role=function(dom){
    	var id_array = new Array();
    	$("#user_role_tab tbody :checkbox").each(function(i){
    		if($(this).is(":checked")){
    			id_array.push($(this).val())	
    		}  
    	});
    	if(id_array.length<=0){
    		alert("请选择角色！");
    		return;
    	}
    	$.ajax({
    		url:"${ROOT}/sys/user_role/post/",
    		type:"POST",
    		dataType:"json",
    		data:"ids="+id_array+"&userId="+$("#userId").val(),
    		success:function(json){
    			if(json.success){
    				alert("保存成功！");
        			$("#editUserRoleModal").modal("hide");
    			}else{
    				alert("保存失败！");
    			}
    		}
    	});
    }
});


window.editjgxx = function () {
	var id = $("#id").val();
	$.ajaxSettings.async = false; 
	$.getJSON("${ROOT}/sys/user/getByjgId/" + id, function (json) {
		$("#user_jg_tab tbody :checkbox").each(function(){
			$(this).removeAttr("checked");
    	});
		 
		debugger;
		 var content = json.data;
		
		$("#user_jg_tab tbody :checkbox").each(function(){
			var the = $(this);
			for(var i = 0;i<content.length;i++){
				if(the.val()==content[i].insuOrgId){
					$(this).prop("checked","checked");
				}
		}
   	}); 
	    $("#editjgModal").modal();
	});	
}

function selectAll(name){
	
	
	
}



function xzjgid(){
	
	if($("#person_level").val()=="3"){
		$("#query_condition").val($("#insuOrgIdSelect").val());
	}
	
	if($("#person_level").val()=="5"){
		$("#dxjgdiv").show();
		$("#query_condition").val("0");
		
	}else{
		$("#dxjgdiv").hide();
		$("#query_condition").val("");
	}
	
	
}


var TempArr=[];//存储option  
  
$(function(){  
    /*先将数据存入数组*/  
    $("#insuOrgIdSelect option").each(function(index, el) {  
    	debugger;
        TempArr[index] = [$(this).text(),$(this).val()];  
    });  
    $(document).bind('click', function(e) {    
        var e = e || window.event; //浏览器兼容性     
        var elem = e.target || e.srcElement;    
        while (elem) { //循环判断至跟节点，防止点击的是div子元素     
            if (elem.id && (elem.id == 'insuOrgIdSelect' || elem.id == "makeupCo")) {    
                return;    
            }    
            elem = elem.parentNode;    
        }    
        $('#insuOrgIdSelect').css('display', 'none'); //点击的不是div或其子元素     
    });    
})  
  
function changeF(this_) {  
    $(this_).prev("input").val($(this_).find("option:selected").text());  
    $("#insuOrgIdSelect").css({"display":"none"});  
}  
function setfocus(this_){  
    $("#insuOrgIdSelect").css({"display":""});  
    var select = $("#insuOrgIdSelect");  
    for(i=0;i<TempArr.length;i++){  
        //var option = $("<option></option>").text(TempArr[i][0]);  
        var option = "<option value='"+TempArr[i][1]+"'>"+TempArr[i][0]+"</option>";
     //   option.value(TempArr[i][1]);
        select.append(option);  
    }   
}  
  
function setinput(this_){  
    var select = $("#insuOrgIdSelect");  
    select.html("");  
    for(i=0;i<TempArr.length;i++){  
        //若找到以txt的内容开头的，添option  
    //    if(TempArr[i].substring(0,this_.value.length).indexOf(this_.value)==0){  
    	if(TempArr[i][0].indexOf(this_.value)>=0){
           // var option = $("<option></option>").text(TempArr[i][0]); 
            var option = "<option value='"+TempArr[i][1]+"'>"+TempArr[i][0]+"</option>";
        //    option.value(TempArr[i][1]);
            select.append(option);  
        }  
    }  
}  
    
    
    
    
    function  keep_close(){
    	
    	
    	var id_array = new Array();
    	$("#user_dxjg_tab tbody :checkbox").each(function(i){
    		if($(this).is(":checked")){
    			id_array.push($(this).val())	
    		}  
    	});
    	if(id_array.length<=0){

    	}
    	 
    	   $.ajax({
       		url: "${ROOT}/sys/user/post/",      		
       		type:"POST",
       		dataType:"json",
       		data:"id="+$("#id").val()+"&ids="+id_array+"&username="+$("#username").val()+"&password="+$("#password").val()+"&insuOrgIdSelect="+$("#insuOrgIdSelect").val()+"&person_level="+$("#person_level").val()+"&query_condition="+$("#query_condition").val()+"&flag="+"1",
       		success:function(json){
       		}
       	});
    	   
    	
    }
    
    
    function jgmhfind(obj){
    	
    	  var mytable = document.getElementById('user_dxjg_tab');
    	  for(var i=1;i<mytable.rows.length;i++){
    	     if(mytable.rows[i].cells[2].innerHTML.indexOf(obj.value)>=0||obj==null||obj.value.length==0){
    	    	 mytable.rows[i].style.display = "block";
    	     }else{
    	    	 mytable.rows[i].style.display = "none";
    	     }
    	  }
    	   
    }
    
    
    
    	

</script>
</html>
	