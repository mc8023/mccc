<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<%@include file="/WEB-INF/comm/_env.jsp"%>
	<title>东风 | 菜单管理</title>
	<style type="text/css">
#areaid.form-control #pbk.form-control {
	width: 100px;
	align: center;
}
</style>
</head>
<body id="main-content" style="overflow:-Scroll;overflow-x:hidden" onload="javascript:load();">
		<div class="container">
		<div class="row">
				<div id="content" class="col-lg-12">
				
					<!-- PAGE HEADER-->
					<div class="row">
						<div class="page-header">
							<ul class="breadcrumb">
							</ul>
							<form class="form-inline" id="fm" role="form"
								action="${ROOT}/settlementDetail/settlement_Detail_Query.html" >
								<div class="box">
									
										<div class="box">
										<input type="hidden" id="pleve" name="pleve" value="${plevels}"/>
										<input type="hidden" id="pcon" name="pcon" value="${pconds}"/>
										<input type="hidden" id="lplxhid" name="lplxhid" value="${lplxhidden}"/>
											<div class="form-group" style="margin-left: 50px;">
												<label class="control-label info">日期:</label>
											</div>
											<div class="form-group">
												<div class="date_input_div_begin">
												<input type="hidden" id="checkdate" name="checkdate" value="${pstardate}"/>
													<input name="beginClinDate" id="beginClinDate" type="text"
														placeholder="开始日期 " class="Wdate form-control"
														style="height: 34px;"
														onfocus="WdatePicker({dateFmt:'yyyy-MM-dd',minDate:'1990y-01M-01d',maxDate:'%y-%M-%d'})"
														value="${pstardate}"/>
												</div>
											</div>
											<div class="form-group">
												<div class="date_input_div_begin">
												<input type="hidden" id="checkeddate" name="checkeddate" value="${penddate}"/>
													<input name="endClinDate" id="endClinDate" type="text"
														placeholder="结束日期 " class="Wdate form-control"
														style="height: 34px;"
														onfocus="WdatePicker({dateFmt:'yyyy-MM-dd',minDate:'1990y-01M-01-d',maxDate:'%y-%M-%d'})"
														value="${penddate}"/>
												</div>
											</div>
											<div class="form-group">
												  <div class="list1" id="list1"
													style="padding: 0px; margin:0px;">
													<label class="control-label info">属地:</label> 
													<input type="hidden" id="belongarea" name="belongarea" value="${areahidden}"/>
													<input type="hidden" class="form-control" name="areaid" id="areaid"/>
													<input type="text" class="form-control" name="areaid1" id="areaid1" readonly="true" style="width: 150px;" value="${foo}">
													</input>
														<div class="select1" id="select1"
															style="border: 1px solid #CCCCCC; display: none; background: #EEEEEE; position: fixed; z-index: 7; min-width: 150px; margin-left: 38px;">
															<ul id="list_sd" style="padding-left:15px;" >
																<c:forEach items="${sd}" var="item">
																	<li style="list-style-type:none;"><input
																		type="checkbox" value="${item.item_value}"
																		name="check1" class="chk" />${item.item_nm}</li>
																</c:forEach>
															</ul>
														</div>
												</div>
											</div>
											<div class="form-group" style="margin-left:5px;" id="bkdiv" >
												<div class="list" id="list"
													style="padding: 0px; margin: 0px;">

													<label class="control-label info">板块:</label>
													<input type="hidden" id="bblock" name="bblock" value="${pbkhidden}"/>
													 <input type="hidden" class="form-control" name="pbk" id="pbk"style="width: 140px;"/>
													 <input type="text" class="form-control" id="pbk1" name="pbk1" readonly="true" style="width: 150px;" />
													<div class="select" id="select"
														style="border: 1px solid #CCCCCC; display:none; background: #EEEEEE; position: fixed; z-index:7; min-width: 150px;margin-left:39px;">
														<ul style="padding-left:15px;">
															<c:forEach items="${bk11}" var="item">
																<li style="list-style-type:none;">
																<input type="checkbox" value="${item.insu_org_id}" name="check" class="chk" />${item.insu_org_nm_ab}</li>
															</c:forEach>
														</ul>
													</div>
												</div>
											</div>
												<div class="form-group" style="margin-left:5px;">
													<label class="control-label info">理赔类型:</label> 
													
													<select name="lplx" id="lplx" class="form-control" style="width:150px;text-align:center;">
													<!-- <option value="-1" >全部</option>
													<option value="1">线下理赔</option>
													<option value="2">医院直赔</option> -->
												    <option class="lplxitem" value="-1">全部</option>
												    <option class="lplxitem" value="1">线下理赔</option>
													<option class="lplxitem" value="2">医院直赔</option>
													</select>
												</div>
												<div class="form-group" style="margin-left:50px;">
													<label class="control-label info">单位:</label> 
													<input id="insu_org_nm" class="form-control" name="insu_org_nm" style="width: 140px;" value="${dw}"></input>
												</div>
											<div class="form-group" style="margin-left:50px;">
												<label class="sr-only"></label>
												<div class="chaxun_input_div">
													<button type="submit" class="btn btn-primary-cx" id="submitfm" onclick="query();">
														查询
													</button>
													<button type="button" class="btn btn-primary-xz" onClick="daochuInfo()" id="daochu" >导出 </button>
													<!-- <a type="button"  class="btn btn-primary-xz" id="daochu" onclick="daochuInfo();" href="">导出</a> -->
													
												</div>
											</div>
										</div>
							</form>
						</div>
					</div>
				
				<div class="row">
					<div class="col-md-12">
						<div class="box border green" style="padding: 0px 0px 0px 0px;">
							<div class="box-title">
								<h4>
									<i class="fa fa-table"></i>
									
									<c:if test="${not empty pstardate}">
									<span style="display: inline-block; width: 200px;font-size:15px;">
									${pstardate}~${penddate}
									</span>
									</c:if>
									员工福利理赔明细表
									<c:if test="${not empty areahidden}">
									<span style="display: inline-block;width:250px;" id="areaidinfo">
									属地：
									<c:forEach items="${sd}" var="item">
										<c:forEach items="${areahidden}" var="items">
											<c:if test="${item.item_value == items }">
												<span style="margin-left: 5px; display: inline; ">${item.item_nm }</span>
											</c:if>
										</c:forEach>
									</c:forEach>
									</span>
									</c:if>
									<c:if test="${not empty pbkhidden}">
									<span style=" display: inline-block;" id="bkbkinfo">
									板块：
									<c:forEach items="${bk11}" var="item">
										<c:forEach items="${pbkhidden}" var="items">
											<c:if test="${item.insu_org_id == items }">
												<span style="margin-left: 5px; display: inline;">${item.insu_org_nm_ab }</span>
											</c:if>
										</c:forEach>
									</c:forEach>
									</span>
									</c:if>
								</h4>
							</div>
							<div class="box-body" style="overflow-x: auto; overflow-y: auto;">
								<table id="datatable" style="font-size: 13px;width:3000px;"
									cellpadding="0" cellspacing="0" border="0"
									class="datatable table table-striped table-bordered table-condensed table-hover">
									<thead>
										<tr>
											<th class="center" rowspan="2" align="center" valign="middle">序号</th>
											<th class="center" rowspan="2">属地</th>
											<th class="center" rowspan="2">流水号</th>
											<th class="center" rowspan="2">板块名称</th>
											<th class="center" rowspan="2">单位名称</th>
											<th class="center" rowspan="2">申请人姓名</th>
											<th class="center" rowspan="2">出险人姓名</th>
											<th class="center" rowspan="2">出险人身份证号</th>
											<th class="center" rowspan="2">人员类别</th>
											<th class="center" rowspan="2">申请金额</th>
											<th class="center" rowspan="2">第三方赔付金额</th>
											<th class="center" rowspan="2">扣减金额</th>
											<th class="center" rowspan="2">赔付比例</th>
											<th class="center" rowspan="2">赔付金额</th>
											<th class="center" rowspan="2">出险日期</th>
											<th class="center" rowspan="2">申请日期</th>
											<th class="center" rowspan="2">结案时间</th>
											<th class="center" rowspan="2">划账日期</th>
											<th class="center" rowspan="2">理赔时效</th>
											<th class="center" rowspan="2">超时效天数</th>
											<th class="center" rowspan="2">超时效原因</th>
											<th class="center" colspan="2">备注</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${pageResult.pageList }" var="item">
											<tr>
												<td class="center order">${item.rownums }</td>
												<td class="center caseId">${item.itemnms }</td>
												<td class="center">${item.case_id }</td>
												<td class="center">${item.bk }</td>
												<td class="center">${item.insu_org_nm }</td>
												<td class="center">${item.poli_hold_nm }</td>
												<td class="center">${item.insu_nm }</td>
												<td class="center">${item.id_card }</td>
												<td class="center">${item.insu_type }</td>
												<td class="center">${item.sqje }</td>
												<td class="center">${item.third_pay }</td>
												<td class="center">${item.cut_pay }</td>
												<td class="center">${item.comp_rate }</td>
												<td class="center">${item.comp_pay }</td>
												<td class="center">${item.happen_date }</td>
												<td class="center">${item.rece_date }</td>
												<td class="center">${item.close_date }</td>
												<td class="center">${item.REMIT_DATE }</td>
												<td class="center">${item.effect_date }</td>
												<td class="center">${item.cssj }</td>
												<td class="center"> </td>
												
												<td class="center"> 
												 ${item.bzxx }
												 
												</td>
												
												
											</tr>
										</c:forEach>
								
									</tbody>
								</table>
								<div id="paging1" class="page"
									style="margin-top: 1%; left: 20px;"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			</div>
		</div>
</body>
<script type="text/javascript" charset="utf-8">
// $(function () {
//     var h1 = 0; 
//     $('#datatable tr').each(function () {
//             $(this).find('td:eq(9)').each(function(){
//             	h1 += parseFloat($(this).text());
//             });
//     });
// 		$('#totalRow').html(h1.toFixed(2));
// });
	$(function() {
		
		var paramStr1="";
		if($('#checkdate').val()!=""){
			paramStr1 = "beginClinDate1="+$("#checkdate").val()+"&";
		}
		
		var paramStr6="";
		if($('#checkeddate').val()!=""){
			paramStr6 = "endClinDate1="+$("#checkeddate").val()+"&";
		}
		
		var paramStr2="";
		if($("#belongarea").val()!=""){
			paramStr2 = "areaids="+$("#belongarea").val()+"&";
		}
		
		var paramStr3="";
		if($("#bblock").val()!=""){
			paramStr3 = "bks="+$("#bblock").val()+"&";
		}
		var paramStr4="";
		if($("#lplxhid").val()!=""){
			paramStr4 = "lplxs="+$("#lplxhid").val()+"&";
		}
		var paramStr5="";
		if($("#insu_org_nm").val()!=""){
			paramStr5 = "insu_org_nm="+$("#insu_org_nm").val()+"&";
		}
		$("#paging1")
				.pagination(
						{
							items : parseInt('${pageResult.pages}'),
							currentPage : parseInt('${pageResult.pageNum}'),
							cssStyle : "light-theme",
							prevText : "上一页",
							nextText : "下一页",
							hrefTextPrefix : "${ROOT}/settlementDetail/settlement_Detail_Query.html?"+paramStr1+paramStr6+paramStr2+paramStr3+paramStr4+paramStr5+"pageSize=${pageResult.pageSize}&total=${pageResult.total}&pageNum="
							
						});
	});


	$(function() {
		//设置板块的选择
		var bk = $("#pbk1");
		bk.click(function() {
			$(".select").slideToggle("slow");
		});
		var sd = $("#areaid1");
		sd.click(function() {
			$(".select1").slideToggle("slow");
		});
		//下拉框的高度大于180px时，显示滚动条，反之隐藏
		var iTarget = 150;
		var sel_h = $("#select").height();
		var sel_h1 = $("#select1").height();
		if (sel_h > iTarget) {
			$("#select").css({
				"height" : "150px",
				"overflow-y" : "scroll"
			});
		}
		if (sel_h1 > iTarget) {
			$("#select1").css({
				"height" : "150px",
				"overflow-y" : "scroll"
			});
		}
	});

	//点击其他地方隐藏
	$(document).bind("click", function(event) {
		var e = event || window.event;
		var w = event || window.event;
		var elem =e.target;
		var targets =w.target;
		while (elem) {
			if (elem.id == "list") {
				return;
			}
			if(elem.id == "list1"){
				return;
			}
			elem = elem.parentNode;
		}
		while(targets){
			if(targets.id == "list1"){
				return;
			}
			targets = targets.parentNode;
		}
		ensure();
		$('#select').hide( "slow" );
		$('#select1').hide( "slow" );
		
	});
	//判断是否选中checkbox的值
	function ensure() {
		var result = [];
		var resultValue = [];
		var result1 = [];
		var resultValue1 = [];
		var check_array = document.getElementsByName("check");
		var check_array1 = document.getElementsByName("check1");
		for (i in check_array) {
			if (check_array[i].checked){
				result.push(check_array[i].nextSibling.nodeValue);
				resultValue.push(check_array[i].value);
			}
		
		}
		if(result!=""){
			$("#pbk1").val(result);
		}
		
		$("#pbk").val(resultValue);
		
		for (j in check_array1) {
			if (check_array1[j].checked) {
				result1.push(check_array1[j].nextSibling.nodeValue);
				resultValue1.push(check_array1[j].value);
			}
		
		}
		if(result1 !=""){
			$("#areaid1").val(result1);
		}
			
			$("#areaid").val(resultValue1);
			
	};
	
	
	function query(){
		var rq1 = $("[name=checkdate]").val().replace(/-/g,"");
		var rq2 = $("[name=checkeddate]").val().replace(/-/g,"");
		
		if(rq1>rq2){
			alert("开始时间不能大于结束时间！");
		}
		
	}
	
	
	function daochuInfo(){
		var rqs="";
		if($('#checkdate').val()!=""){
			rqs =$("#checkdate").val();
		}
		var rq1s="";
		if($('#checkeddate').val()!=""){
			rq1s =$("#checkeddate").val();
		}
		var sds="";
		if($("#belongarea").val()!=""){
			sds =$("#belongarea").val();
		}
		
		var bks="";
		if($("#bblock").val()!=""){
			bks =$("#bblock").val();
		}
		
		var lplxs="";
		if($("#lplxhid").val()!=""){
			lplxs = $("#lplxhid").val();
		}
		var dw="";
		if($("#insu_org_nm").val()!=""){
			dw =$("#insu_org_nm").val();
		}
		 /* 采用post提交方式 */
		var link="${ROOT}/exportExcel/exportSttlementDetail?";
		/* 首先创建一个form表单   */
		var tempForm = document.createElement("form");    
		tempForm.id="tempForm1";
		/* 制定发送请求的方式为post   */
		tempForm.method="post";   
		/* 此为window.open的url，通过表单的action来实现   */
		tempForm.action=link;  
		/* 利用表单的target属性来绑定window.open的一些参数（如设置窗体属性的参数等） */  
		tempForm.target="_parent";
		/* 创建input标签，用来设置参数   */
        var hideInput1 = document.createElement("input");    
        hideInput1.type="hidden";    
        hideInput1.name= "beginClinDate1";  
        hideInput1.value= rqs;
        /* 创建input标签，用来设置参数   */
        var hideInput2 = document.createElement("input");    
        hideInput2.type="hidden";    
        hideInput2.name= "endClinDate1";  
        hideInput2.value= rq1s;
        /* 创建input标签，用来设置参数   */
        var hideInput3 = document.createElement("input");    
        hideInput3.type="hidden";    
        hideInput3.name= "dws";  
        hideInput3.value= dw;
        /* 创建input标签，用来设置参数   */
        var hideInput4 = document.createElement("input");    
        hideInput4.type="hidden";    
        hideInput4.name= "areaids";  
        hideInput4.value= sds;
        /* 创建input标签，用来设置参数   */
        var hideInput5 = document.createElement("input");    
        hideInput5.type="hidden";    
        hideInput5.name= "bks";  
        hideInput5.value= bks;
        /* 创建input标签，用来设置参数   */
        var hideInput6 = document.createElement("input");    
        hideInput6.type="hidden";    
        hideInput6.name= "lplxss";  
        hideInput6.value= lplxs;
        /*  将input表单放到form表单里   */
        tempForm.appendChild(hideInput1); 
		tempForm.appendChild(hideInput2); 
		tempForm.appendChild(hideInput3); 
		tempForm.appendChild(hideInput4); 
		tempForm.appendChild(hideInput5); 
		tempForm.appendChild(hideInput6); 
        /* 将此form表单添加到页面主体body中   */
        document.body.appendChild(tempForm); 
		/* 手动触发，提交表单   */
        tempForm.submit();
		/* 从body中移除form表单 */  
        document.body.removeChild(tempForm);  
		
		/* var url="${ROOT}/exportExcel/exportSttlementDetail?beginClinDate1="+rqs+"&endClinDate1="+rq1s+"&dws="+dw+"&areaids="+sds+"&bks="+bks+"&lplxss="+lplxs; 
		$("#daochu").attr("href",url);  */
		
	}
$(function(){
	if($("#areaidinfo").find("span").text()!=""){
	var str="";
	str = $("#areaidinfo").find("span").text();

	$("#areaid1").val(str);
	}
	if($("#bkbkinfo").find("span").text()!=""){
		var strr="";
		strr = $("#bkbkinfo").find("span").text();
		$("#pbk1").val(strr);
	}
// 	if($("#lplxs").val()!=null && $("#lplxs").val()!=""){
// 		$("#lplx").val($("#lplxs").val());
// 	}
});



function load	(){
		if($("#pleve").val()!=null && $("#pleve").val()!=""){
			if($("#pleve").val()=="2"||$("#pleve").val()=="3"||$("#pleve").val()=="4"){
				$("#bkdiv").css("display","none");
			}
		}

	var adval = new Array;
	var bkval = new Array;
	var ad = '<%=request.getAttribute("areahidden") %>';
	var bk = '<%=request.getAttribute("pbkhidden") %>';
	var lplxlx = '<%=request.getAttribute("lplxhidden") %>';
	adval = ad.split(",");
	
	for(var j=0;j<adval.length;j++){
    $("input[name='check1']").each(function(i){
		if($(this).val()==adval[j]){
			$(this).prop("checked", true); 
		}
    });}
	
	if(null!=bk && ''!=bk){
		bkval = bk.split(",");
		for(var j=0;j<bkval.length;j++){
		    $("input[name='check']").each(function(i){
				if($(this).val()==bkval[j]){
					$(this).prop("checked", true); 
				}
		    });}
	}
	if(null!=lplxlx && ''!=lplxlx){
	$("option[class='lplxitem']").each(function(i){
		if($(this).val()==lplxlx){
			$(this).attr("selected",true);
		}
	})
	}
};


</script>
</html>
