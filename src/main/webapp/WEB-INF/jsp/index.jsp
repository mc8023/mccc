<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>MC中心管理系统</title>
    	<style type="text/css">
		* { margin: 0; padding: 0; }
		html,body { font: 13px Verdana,SimSun; width: 100%; height: 100%;}
	</style>
	<!--[if lt IE 9]>
	<?import namespace="v" implementation="#default#VML" ?>
	<![endif]-->
	<%@include file="/WEB-INF/comm/_env.jsp"%>
	<style>
  .right_Click_menu{
    border-radius: 4px;
    text-align: left;
    line-height: 25px;
    padding: 7px 0;
    white-space: nowrap;
    position: absolute!important;
    left: 0;
    top: 0;
    z-index: 1000;
    border: 1px solid #bbb;
    box-shadow: 0 1px 3px rgba(0,0,0,.2);
    background: #fff;
    cursor: default;
    outline: 0;
    display:none;
  }
  
  .nui-menu-item {
    position: relative;
    zoom: 1;
    cursor: pointer;
    outline: 0;
    
  }
  .nui-menu-item-inner, .nui-menu-item-link {
    display: block;
    text-decoration: none;
    padding: 0 52px 0 32px;
    position: relative;
    zoom: 1;
}

.nui-menu-item:hover>.nui-menu-item-link{
    background-color: #CDE3F3;
}
</style>
  </head>

  <body style="overflow: hidden;">
  
  <div class="right_Click_menu" id="right_Click_menu">
    <div role="menuitem" tabindex="-1" id="_mail_menuitem_0_192" class="js-component-menuitem nui-menu-item ">
		<div class="nui-menu-item-link">
			<span class="nui-menu-item-text">刷新</span>
		</div>
	</div>
	<div role="menuitem" tabindex="-1" id="_mail_menuitem_0_193" class="js-component-menuitem nui-menu-item ">
		<div class="nui-menu-item-link">
			<span class="nui-menu-item-text">关闭当前窗口</span>
		</div>
	</div>
	<div role="menuitem" tabindex="-1" id="_mail_menuitem_1_194" class="js-component-menuitem nui-menu-item ">
		<div class="nui-menu-item-link">
			<span class="nui-menu-item-text">关闭全部</span>
		</div>
	</div>
	<div role="menuitem" tabindex="-1" id="_mail_menuitem_2_195" class="js-component-menuitem nui-menu-item ">
		<div class="nui-menu-item-link">
			<span class="nui-menu-item-text">关闭其它全部</span>
		</div>
	</div>
 </div>
   <iframe src="${ROOT}/homePage/page" id="pageIframe" name="pageIframe" marginheight="0" marginwidth="0" frameborder="0" scrolling="no" width="100%" height="100%"></iframe>
  </body>
  <script type="text/javascript">



        var modular_obj;
   
		$(function() {
			$(document).bind("contextmenu", function(e) {
				return false;
			});
		});
		function show_menu(obj, x, y) {
			modular_obj = obj;
			$("#right_Click_menu").css("left", x).css("top", y).show();
		}
		
		$("#_mail_menuitem_0_192").click(function(){
			var modular_name = $(modular_obj).find("span").text()
			var parent_iframe_div = $("#pageIframe").contents().find("#leftFrame").contents().find("#parent_iframe_div");
			var cur_iframe = parent_iframe_div.children("div[name='"+modular_name+"_iframe']").children("iframe");
			cur_iframe.attr('src', cur_iframe.attr('src'));
			$("#right_Click_menu").hide();
		});
		
		$("#_mail_menuitem_0_193").click(function(){
			var modular_name = $(modular_obj).find("span").text()
			var parent_iframe_div = $("#pageIframe").contents().find("#leftFrame").contents().find("#parent_iframe_div");
			var cur_iframe_div = parent_iframe_div.children("div[name='"+modular_name+"_iframe']");
			cur_iframe_div.remove();
			var last_header_li = $(modular_obj).siblings(":last");
			modular_obj.remove();
			last_header_li.trigger("click");
			$("#right_Click_menu").hide();
		});
		
		$("#_mail_menuitem_1_194").click(function(){
			
		});
		
		$("#_mail_menuitem_2_195").click(function(){
		});
		
  </script>
</html> 