<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<%@include file="/WEB-INF/comm/_env.jsp"%>
<title>东风 | 菜单管理</title>
	<style>
        .sel_btn{
            height: 30px;
            line-height: 21px;
            padding: 0 11px;
            background: #26bbdb;
            border: 1px #000000 solid;
            border-radius: 3px;
            /*color: #fff;*/
            display: inline-block;
            text-decoration: none;
            font-size: 18px;
            outline: none;
        }
        p{
        font-size:12px;	
        	}
        #id_card{
        	overflow:hidden;
        	white-space:nowrap;
	        text-overflow:ellipsis;
	        width:200px;
        	}
        #id_card:hover{
        overflow:visible;
        white-space:pre-wrap;
        }	
    </style>
</head>

<script type="text/javascript">
$(function(){
	if($("#dftblx").val()=="2"){
		$("#fmxx1").show();
		$("#fmxx2").show();
		$("#ygxx").hide();
	}else{
		$("#fmxx1").hide();
		$("#fmxx2").hide();
		$("#ygxx").show();
		
	}
	
	
});	
</script>
<body>

			<div id="main-content" >
			<div class="container">
				<div class="row">
					<div id="content" class="col-lg-12">
						<!-- PAGE HEADER-->
						<div class="row">
							<div class="col-sm-12">
								<div class="page-header">
									<ul class="breadcrumb">
									</ul>
									<div class="modal-header">
									<span class="icono-cross" id="fpxqff" data-dismiss="modal" style="display:inline-block;float:right;margin-top:-15px;color:#7e7e7e;"/>

									<!-- <h4 class="modal-title" id="myModalLabel2">发票详情</h4>-->
									</div>
									<form class="form-inline" role="form" action="${ROOT}/baoquan/baoquanInfo.html">
										<input type="hidden" name="dftblx" id="dftblx" value="${baoquanInfo.dftblx}"/>
										<input type="hidden" name="nf1" id="nf1" value="${baoquanInfo.nf}"/>
										<input type="hidden" name="insuid1" id="insuid1" value="${baoquanInfo.insuid}"/>
										<input id="insu_id" value="${baoquanInfo.insu_id}"  type="hidden"/>
										
										<div id="tips" align="center"></div>
											<div class="modal-body">
												<div class="form-group col-sm-12">
													<div class="col-sm-3 xszy">
													<p >年度：${baoquanInfo.nf}</p>
													</div>
													<div class="col-sm-4 xszy">
													<p >投保类型：${baoquanInfo.tblx}</p>
													</div>
													<div class="col-sm-3 xszy">
													<p >被保险人编码：${baoquanInfo.insu_id}</p>
													</div>
													<div class="col-sm-2 xszy">
													<p >被保险人姓名：${baoquanInfo.insu_nm}</p>
													</div>
												</div>
												<div class="form-group col-sm-12 " >
													<div class="col-sm-3 xszy">
													<p >出生日期：${baoquanInfo.csrq}</p>
													</div>
													<div class="col-sm-4 xszy">
													<p id="id_card">身份证号：${baoquanInfo.sfzh}</p>
													</div>
													<div class="col-sm-3 xszy">
													<p >性别：${baoquanInfo.sex}</p>
													</div>
													<div class="col-sm-2 xszy">
													<p >是否投保：是</p>
													</div>
												</div>
												
												<div class="form-group col-sm-12">
													<div class="col-sm-3 xszy">
													<p >投保属地：${baoquanInfo.tbsd}</p>
													</div>
													<div class="col-sm-4 xszy">
													<p >居住地：${baoquanInfo.jzd}</p>
													</div>
													<div class="col-sm-3 xszy">
													<p >特殊门诊：${baoquanInfo.tsmz}</p>
													</div>
													<div class="col-sm-2 xszy">
													<p >劳保证号：${baoquanInfo.lbzh}</p>
													</div>
												</div>
												<div class="form-group col-sm-12" id="fmxx1">
													<div class="col-sm-3 xszy">
													<p ><span style="color:red">F</span>是否退保：</p>
													</div>
													<div class="col-sm-4 xszy">
													<p ><span style="color:red">F</span>员工姓名：${baoquanInfo.fygxm}</p>
													</div>
													<div class="col-sm-3 xszy">
													<p ><span style="color:red">M</span>是否退保：</p>
													</div>
													<div class="col-sm-2 xszy">
													<p ><span style="color:red">M</span>员工姓名：${baoquanInfo.mygxm}</p>
													</div>
												</div>
												
												
												
												
												<div class="form-group col-sm-12">
													<div class="col-sm-3 xszy">
													<p >亲属关系：${baoquanInfo.qsgx}</p>
													</div>
													<div class="col-sm-4 xszy">
													<p >门诊基本保额：${baoquanInfo.mzjbbe}</p>
													</div>
													<div class="col-sm-3 xszy">
													<p >住院基本保额：${baoquanInfo.zyjbbe}</p>
													</div>
												</div>
												<div class="form-group col-sm-12" id="fmxx2">
													<div class="col-sm-3 xszy">
													<p ><span style="color:red">F</span>单位名称：${baoquanInfo.fdwmc}</p>
													</div>
													<div class="col-sm-4 xszy">
													<p ><span style="color:red">M</span>单位名称：${baoquanInfo.mdwmc}</p>
													</div>
													<div class="col-sm-3 xszy">
													<p ><span style="color:red">F</span>员工编码：${baoquanInfo.fygbm}</p>
													</div>
													<div class="col-sm-2 xszy">
													<p ><span style="color:red">M</span>员工编码：${baoquanInfo.mygbm}</p>
													</div>
												</div>
												
												<div class="form-group col-sm-12" id="ygxx">
													<div class="col-sm-3 xszy">
												    <p >员工姓名：${baoquanInfo.ygxm}</p>
													</div>
													<div class="col-sm-4 xszy" >
													<p >员工编码：${baoquanInfo.ygbm}</p>
													</div>
													<div class="col-sm-5 xszy">
													<p >单位名称：${baoquanInfo.dwmc}</p>
													</div>
												</div>
												<div class="form-group col-sm-12">
													<div class="col-sm-3 xszy">
													<p >门诊剩余赔付：${baoquanInfo.mzsypf}</p>
													</div>
													<div class="col-sm-4 xszy">
													<p >住院剩余赔付：${baoquanInfo.zysypf}</p>
													</div>
													<div class="col-sm-3 xszy">
													<p >意外伤害保险基准金额：${baoquanInfo.ywshjzbe}</p>
													</div>
													<div class="col-sm-2 xszy">
													<p >开始日期：${baoquanInfo.ksrq}</p>
													</div>
												</div>
												<div class="form-group col-sm-12">
													<div class="col-sm-3 xszy">
													<p >意外伤害剩余赔付金额：${baoquanInfo.ywshsypfje}</p>
													</div>
													<div class="col-sm-4 xszy">
													<p >意外医疗剩余赔付金额：${baoquanInfo.ywylsypfje}</p>
													</div>
													<div class="col-sm-3 xszy">
													<p >意外医疗基准保额：${baoquanInfo.ywyljzbe}</p>
													</div>
													<div class="col-sm-2 xszy">
													<p >结束日期：${baoquanInfo.jsrq}</p>
													</div>
												</div>
											</div>
									</form>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="form-group" >
								<%-- <a id="bqdc"  class="btn btn-primary"  type="button" href="${ROOT}/updownload/baoquanxinxi?arrayObj=${baoquanInfo.insu_id}-${baoquanInfo.nf}" >导出</a> --%>
								<a id="bqdc"  class="btn btn-primary"  type="button" href="javascript:exportData()" >导出</a>
							</div>
						</div>
						
						<div class="row">
							<div lass="col-md-12">
								<div class="box border green" style="padding: 0px 0px 1% 0px;">
									<div class="box-title">
										<h4>
											<i class="fa fa-table"></i>保全信息
										</h4>
									</div>
									<div class="box-body" style="overflow-x: auto; overflow-y: auto; ">
										<table id="datatable" style="font-size: 12px;width:4000px;" cellpadding="0"
											cellspacing="0" border="0"
											class="datatable table table-striped table-bordered table-condensed table-hover"
											>
											<thead>
												<tr>
													<th class="center">序号</th>
													<th class="center">变更流水号</th>
													<th class="center">年份</th>
													<th class="center">投保类型</th>
													<th class="center">变动类型</th>
													<th class="center">状态</th>
													<th class="center">原被保险人姓名</th>
													<th class="center">原员工编码</th>
													<th class="center">原员工姓名</th>
													<th class="center">原工作单位</th>
													<th class="center">原被保险人出生年月</th>
													<th class="center">原被保险人身份证号</th>
													<th class="center">原投保开始日期</th>
													<th class="center">原投保结束日期</th>
													<th class="center">新被保险人姓名</th>
													<th class="center">新员工编码</th>
													<th class="center">新员工姓名</th>
													<th class="center">新工作单位</th>
													<th class="center">新被保险人出生年月</th>
													<th class="center">新被保险人身份证号</th>
													<th class="center">新投保开始日期</th>
													<th class="center">新投保结束日期</th>
													<th class="center">新性别</th>
													<th class="center">新与员工亲属关系</th>
													<th class="center">新投保属地</th>
													<th class="center">新居住地</th>
													<th class="center">增减月数</th>
													<th class="center">录入时间</th>
													<th class="center">录入人员</th>
													<th class="center">审核时间</th>
													<th class="center">审核人员</th>
													<th class="center">备注</th>
												</tr>
											</thead>
											<tbody>
												<c:forEach items="${pageResult.pageList }" var="item">
													<tr>
														<td class="center">${item.xh }</td>
														<td class="center">${item.chan_id }</td>
														<td class="center">${item.nf }</td>
														<td class="center">${item.tblx }</td>
														<td class="center">${item.bdlx }</td>
														<td class="center">${item.status }</td>
														<td class="center">${item.insu_nm_before }</td>
														<td class="center">${item.ygid_before }</td>
														<td class="center">${item.ygnm_before }</td>
														<td class="center">${item.gzdw_before }</td>
														<td class="center">${item.bbrcsny_before }</td>
														<td class="center">${item.bbrsfzh_before }</td>
														<td class="center">${item.tbkssj_before }</td>
														<td class="center">${item.tbjssj_before }</td>
														<td class="center">${item.insu_nm_new }</td>
														<td class="center">${item.ygid_new }</td>
														<td class="center">${item.ygnm_new }</td>
														<td class="center">${item.gzdw_new }</td>
														<td class="center">${item.bbrcsny_new }</td>
														<td class="center">${item.bbrsfzh_new }</td>
														<td class="center">${item.tbkssj_new }</td>
														<td class="center">${item.tbjssj_new }</td>
														<td class="center">${item.sex_new }</td>
														<td class="center">${item.qsgx_new }</td>
														<td class="center">${item.tbsd_new }</td>
														<td class="center">${item.jzd_new }</td>
														<td class="center">${item.zjys }</td>
														<td class="center">${item.lrsj }</td>
														<td class="center">${item.lrry }</td>
														<td class="center">${item.shsj }</td>
														<td class="center">${item.shry }</td>
														<td class="center">${item.bz }</td>
													
														
													</tr>
												</c:forEach>
											</tbody>
										</table>
										<div id="paging1" class="page" style="margin-top: 1%"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<!-- Edit Modal -->
	<div class="modal fade" id="showSettlementModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel"></div>
	<div class="modal fade" id="showBillInfoModal" tabindex="-1" role="dialog"
  		aria-labelledby="myModalLabel2"></div>	
	<div class="modal fade" id="showIllegalInfoModal" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel3"></div>	
	
</body>
<script type="text/javascript" charset="utf-8">
	function exportData(){
		var nf=$("#nf1").val();
		var insu_id=$("#insu_id").val();
		/* 采用post提交方式 */
		var link="${ROOT}/updownload/baoquanxinxi?";
		/* 首先创建一个form表单   */
		var tempForm = document.createElement("form");    
		tempForm.id="tempForm1";
		/* 制定发送请求的方式为post   */
		tempForm.method="post";   
		/* 此为window.open的url，通过表单的action来实现   */
		tempForm.action=link;  
		/* 利用表单的target属性来绑定window.open的一些参数（如设置窗体属性的参数等） */  
		tempForm.target="_parent";
		/* 创建input标签，用来设置参数   */
	    var hideInput = document.createElement("input");    
	    hideInput.type="hidden";    
	    hideInput.name= "arrayObj";  
	    hideInput.value= insu_id+"-"+nf;
	    /*  将input表单放到form表单里   */
	    tempForm.appendChild(hideInput); 
	    /* 将此form表单添加到页面主体body中   */
	    document.body.appendChild(tempForm); 
		/* 手动触发，提交表单   */
	    tempForm.submit();
		/* 从body中移除form表单 */  
	    document.body.removeChild(tempForm);  
	}
	$(function() {
		$('#fpxqff').mouseover(function(){
	   		 $(this).css("color","#515151");
	   	 }); 
	   	 $('#fpxqff').mouseout(function(){
	   		 $(this).css("color","#7e7e7e");
	   	 });
		$("#paging1")
				.pagination(
						{
							items : parseInt('${pageResult.pages}'),
							currentPage : parseInt('${pageResult.pageNum}'),
							cssStyle : "light-theme",
							prevText : "上一页",
							nextText : "下一页",
							hrefTextPrefix : "${ROOT}/baoquan/baoquanInfo.html?pageSize=${pageResult.pageSize}&total=${pageResult.total}&insu_id=${baoquanInfo.insu_id}&nd=${baoquanInfo.nf}&pageNum="
						});
		
		$("a.collapse").click(function(){
			var $parentDiv = $(this).parent().parent();
			if($(this).children("i").hasClass("fa-chevron-down")){
				$(this).children("i").removeClass("fa-chevron-down").addClass("fa-chevron-up");
				$parentDiv.next().show();
				$parentDiv.parent().prev().hide();
			}else{
				$(this).children("i").removeClass("fa-chevron-up").addClass("fa-chevron-down");
				$parentDiv.next().hide();
				$parentDiv.parent().prev().show();
			}
		});
		
	});
	
	

	

	
</script>
</html>
