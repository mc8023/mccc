<%@page pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<!-[if lt IE 9]>
  <script src="http://apps.bdimg.com/libs/html5shiv/3.7/html5shiv.min.js"></script>
  <script src="http://apps.bdimg.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]->
<meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1" />
<!-- <link rel="stylesheet" type="text/css" href="bootstrap.min.css" media="screen"/> -->
<!-- <script type="text/javascript" src="js/respond.min.js"></script> -->
<script type="text/javascript" src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
<!-- <script type="text/javascript" src="js/jquery.placeholder.js"></script> -->

	<%@include file="/WEB-INF/comm/_env.jsp"%>
	<title>MC中心管理系统</title>
</head>
<body>
<!-- SIDEBAR -->
	<div id="sidebar" class="sidebar" style="height:100%;" >
		<div class="sidebar-menu nav-collapse">
		<div class="divide-20" style="margin-top:-36px"></div>
			<!-- <div id="search-bar">
				 <input class="search" type="text" placeholder="Search"><i class="fa fa-search search-icon"></i> 
			</div> -->
			<div id="search-bar" style="margin-top: 15px;height: 50px;">
				<img src="http://192.168.17.169:8080/MCCC/bootstrap_resource/img/userx48.png" style="float: left;margin: 10px 0px 10px 20px;">
				<div style="float: left;margin: 15px 0px 10px 10px;">
					<div style="font-weight: bold;">admin </div>
					<a href="#" data-toggle="InfoModal"><img src="${BSROOT}/img/infox16.png"></a>
					<a href="#" onclick="logout();" data-toggle="modal"><img src="${BSROOT}/img/exitx16.png"></a>
				</div>
			</div>
			<ul>
			
			   <c:forEach items="${funcList}" var="item">
				   <li class="has-sub active" onclick="changeHeaderBar(this)">
				     <c:choose>
				        <c:when test="${empty item.url}">
				            <a href="javascript:;" class="" style="height: 30px;padding: 6px 20px;">
				        </c:when>
				        <c:otherwise>
				            <a href="${ROOT}/${item.url}"  target="mainAreaFrame" style="height: 30px;padding: 6px 20px;">
				        </c:otherwise>
				     </c:choose> 
				     <i  class="fa ${item.icon} fa-fw"></i> <span class="menu-text">${item.name}</span>
				     <c:if test="${fn:length(item.childSysFuncs)!=0}">
				        <span class="arrow open"></span>
				     </c:if>  
						<span class="selected"></span>
						</a>
					 <c:if test="${fn:length(item.childSysFuncs)!=0}">
					    <ul class="sub">
					        <c:forEach items="${item.childSysFuncs}" var="subitem">
							  <li onclick="addHeaderBar(this);"><a class="" href="${ROOT}/${subitem.url}" target="mainAreaFrame"><%-- <i  class="fa ${subitem.icon} fa-fw"> --%></i><span class="sub-menu-text">${subitem.name}</span></a></li>
							</c:forEach>
						</ul>
					 </c:if>	
						
					</li>
			   </c:forEach>
			
				<%-- <li>
					<a href="${ROOT}/index.html">
						<i class="fa fa-tachometer fa-fw"></i> <span class="menu-text">首页</span>
						<span class="selected"></span>
					</a>					
				</li>
				<li class="has-sub active">
					<a href="javascript:;" class="">
					<i class="fa fa-table fa-fw"></i> <span class="menu-text">数据查询</span>
					<span class="arrow open"></span>
					<span class="selected"></span>
					</a>
					<ul class="sub">
						<li><a class="" href="${ROOT}/result/list.html"><span class="sub-menu-text">理赔查询</span></a></li>
					</ul>
				</li>
				
				<li class="has-sub active">
					<a href="javascript:;" class="">
					<i class="fa fa-table fa-fw"></i> <span class="menu-text">系统管理</span>
					<span class="arrow open"></span>
					<span class="selected"></span>
					</a>
					<ul class="sub">
						<li><a class="" href="${ROOT}/sys/user.html" target="mainAreaFrame"><span class="sub-menu-text">用户管理</span></a></li>
						<li><a class="" href="${ROOT}/sys/func.html" target="mainAreaFrame"><span class="sub-menu-text">菜单管理</span></a></li>
						<li><a class="" href="${ROOT}/sys/role.html" target="mainAreaFrame"><span class="sub-menu-text">角色管理</span></a></li>
					</ul>
				</li> --%>
			</ul>
			<!-- /SIDEBAR MENU -->
		</div>
	</div>
		<!-- /SIDEBAR -->
	 <div style="height:100%;width: calc(100% - 200px);margin-left: 200px;" id="parent_iframe_div">
		   <div style="height:100%; width:100%;display:none" name=xx_iframe" >
		    <iframe src="" name="mainAreaFrame" style="height: 100%;width: 100%;" frameborder="0" ></iframe>
		   </div>
		   <div style="height:100%; width:100%;" name="首页_iframe">
		    <iframe src="${ROOT}/homePage/home" name="mainAreaFrame" style="height: 100%;width: 100%;" frameborder="0" onclick="changeHeaderBar(this)"></iframe>
		   </div>
    </div> 
</body>
<script type="text/javascript">

(function($) {
	$.fn.extend({
		//定义鼠标右键方法，接收一个函数参数 
		"rightClick" : function(fn) {
			//调用这个方法后将禁止系统的右键菜单 
			$(document).bind('contextmenu', function(e) {
				return false;
			});
			//为这个对象绑定鼠标按下事件 
			$(this).mousedown(function(e) {
				//如果按下的是右键，则执行函数 
				if (3 == e.which) {
					var x=e.pageX;
					var y=e.pageY;
					fn(this,x,y);
				}
			});
		}
	});
})(jQuery);


 $(window.parent.document).find("#topFrame").contents().find("#sidebar-collapse").click(function(){
	 if($("#sidebar").hasClass("mini-menu")){
		 bigMenu();
	 }else{
		 miniMenu();
	 }
 });
 
 
function bigMenu(){
	 $("#sidebar").removeClass("mini-menu");
	 $("#parent_iframe_div").find("iframe[name='mainAreaFrame']").contents().find("#main-content").removeClass("margin-left-50");
}

function miniMenu(){
	 $("#sidebar").addClass("mini-menu");
	 $("#parent_iframe_div").find("iframe[name='mainAreaFrame']").contents().find("#main-content").addClass("margin-left-50");
} 


function logout(){
	window.parent.location.href="${ROOT}/logout";
	
}

function addHeaderBar(obj){
	var modular_span = $(obj).find("span.sub-menu-text");
	var modularName = modular_span.text();
	var head_modular = $(window.parent.document).find("#topFrame").contents().find("#navbar-left");
	var flag = true;
	head_modular.find("span").each(function(){
		if($(this).text()==modularName){
			//默认点击一次这个头部标签页面
			$(this).trigger("click");
			flag = false;
		}
	});
	if(flag){
		bigMenu();
		var head_modular_bar = $("<li class='dropdown' onclick='showIframe(this);' style='margin-top: 20px;border-left: 2px solid;'>"+
		"<a href='#' class='dropdown-toggle' data-toggle='dropdown' style='padding: 5px;'><i class='fa'></i>"+
		"<span class='name'>"+modularName+"</span><i class='fa glyphicon glyphicon-remove' style='font-size: 10px;margin-left: 10px;' onclick='closeCurModular(this);'></i></a></li>");
		head_modular.append(head_modular_bar);
		head_modular_bar.rightClick(function(obj,x,y){
			parent.parent.window.show_menu(obj,x,y);
		}); 
		$("#parent_iframe_div").children("div").hide()
		var iframe_url=modular_span.parent().attr("href");
		var add_modular_iframe= "<div style='height:100%; width:100%;' name='"+modularName+"_iframe'>"+
	    "<iframe src='"+iframe_url+"' name='mainAreaFrame' style='height: 100%;width: 100%;' frameborder='0' ></iframe></div>";
	    $("#parent_iframe_div").append(add_modular_iframe);
	}
}		


function changeHeaderBar(obj){
	var modularName = $(obj).find("span.menu-text").text();
	if(modularName == '首页'){
       $("#parent_iframe_div").children("div[name='首页_iframe']").show().siblings().hide();

	}
}

</script>
</html>